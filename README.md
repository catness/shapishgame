# About the project

**Shapish** is a shape-matching game with Unicode fonts, which allows to explore the fascinating multitude of language glyphs represented in Unicode. 

Click on the glyphs in the central area to match the selection of 1-5 glyphs that appear in the bottom area. Use the powerups (on the right bar) to get a higher score (represented by the number and color of keys on the top bar). Higher scores mean more glyphs saved in your "glyph dex". Click on the glyph in the dex to see its name and possibly extended information (from an external site).

If the game is too hard in the default mode, use the Development settings to play with various settings.

Make sure to try various character sets in the General settings!

The apk can be downloaded [here](https://bitbucket.org/catness/shapishgame/downloads/). It should work with Android 6.0 and above.

Free resources used in the game: [resources.md](docs/resources.md)

Screenshots: [screenshots directory](screenshots/)


