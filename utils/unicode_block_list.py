#!/usr/bin/python
'''
Converts a Unicode block copy/pasted from Wikipedia (formatted as a table) to the list of raw characters
https://en.wikipedia.org/wiki/Category:Unicode_blocks

'''

import os, sys, re;
reload(sys)
sys.setdefaultencoding('utf-8')

out = ''
filename = sys.argv[1] 
with open(filename,"r") as f:
	for line in f:
		line = ''.join(line.split())
		line = re.sub("^U\\+[0-9a-fA-F]+x","",line)
		#print line
		out += line


print out
