package org.catness.shapishgame;

import android.app.Application;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ProgressBar;

import org.catness.shapishgame.chars.CharDao;
import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.database.DataRepository;
import org.catness.shapishgame.elements.GlyphItem;
import org.catness.shapishgame.elements.GlyphRandomColor;
import org.catness.shapishgame.utils.Glyphs;
import org.catness.shapishgame.utils.SymbolSet;
import org.catness.shapishgame.utils.Symbols;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static org.catness.shapishgame.utils.Glyphs.utf16glyphs;
import static org.catness.shapishgame.utils.Utils.codepointToString;
import static org.catness.shapishgame.utils.Utils.getScreenSize;
import static org.catness.shapishgame.utils.Utils.getUnicodeStartEnd;
import static org.catness.shapishgame.utils.Utils.randomGen;
import static org.catness.shapishgame.utils.Utils.stringToCodepoint;

public class LoadingActivity extends AppCompatActivity {
    private static final String LOG_TAG = LoadingActivity.class.getSimpleName();
    private GridLayout gridLayout;
    private final int boardColumns = 5;
    private final int boardRows = 5;
    private ArrayList<GlyphItem> items;
    private int numchars;
    private String glyphs;
    private int charwidth;
    private static TimerTask myTimerTask = null;
    private ArrayList<GlyphRandomColor> colors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        Symbols symbols = new Symbols(this);
        SymbolSet selected = symbols.select("Symbols");
        this.glyphs = selected.glyphs;
        this.charwidth = selected.charwidth;
        numchars = glyphs.length() / charwidth;

        ProgressBar progressBar = findViewById(R.id.loading_bar);
        int num = Glyphs.glyphs.size();
        progressBar.setMax(num);
        progressBar.setProgress(4);
        gridLayout = findViewById(R.id.layout_grid);
        items = new ArrayList<>();
        colors = new ArrayList<>();
        for (int i = 0; i < boardRows * boardColumns; i++) {
            GlyphItem t = new GlyphItem(this, null, R.attr.charStyle, i);
            t.setTypeface(selected.typeface);
            t.setTextSize(TypedValue.COMPLEX_UNIT_SP, selected.fontsize);
            t.setPadding(selected.hpad, selected.vpad, selected.hpad, selected.vpad);
            GlyphRandomColor c = new GlyphRandomColor();
            t.setColors(c.getColor1(), c.getColor2());
            colors.add(c);
            gridLayout.addView(t);
            items.add(t);
        }
        gridInit();

        new Timer().scheduleAtFixedRate(myTimerTask = new TimerTask() {
            @Override
            public void run() {
                animateGlyphs();
            }
        }, 0, 100);//put here time 1000 milliseconds=1 second

        LoadGlyphs loadGlyphs = new LoadGlyphs(this, progressBar);
        loadGlyphs.execute();
    }

    private void updateLayout(int width, int height) {
        ViewGroup.LayoutParams params = gridLayout.getLayoutParams();
        params.width = width;
        params.height = height;
        gridLayout.setLayoutParams(params);
        gridLayout.setColumnCount(boardColumns);
        gridLayout.requestLayout();
        int paddingGrid = gridLayout.getPaddingLeft();
        float cellSizeLimit = (width / boardColumns) - paddingGrid / 2;
        for (int i = 0; i < boardRows * boardColumns; i++) {
            items.get(i).setCellSizeLimit(cellSizeLimit);
            items.get(i).setMinWidth((int) cellSizeLimit);
            items.get(i).setWidth((int) cellSizeLimit);
        }
    }

    private void gridInit() {
        int id = 0;
        String s;
        for (int row = 0; row < boardRows; row++) {
            for (int col = 0; col < boardColumns; col++) {
                s = newBoardChar();
                items.get(id).updateFast(s);
                id++;
            }
        }
    }

    private void animateGlyphs() {
        this.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        int id = randomGen.nextInt(boardColumns * boardRows);
                        String s = newBoardChar();
                        items.get(id).update(s);

                        for (int i = 0; i < boardRows * boardColumns; i++) {
                            if (i == id) continue;
                            GlyphRandomColor c = colors.get(i);
                            c.update();
                            GlyphItem item = items.get(i);
                            item.setColors(c.getColor1(), c.getColor2());
                            item.updateFast(item.getText().toString());
                            //if (i==0) Log.d(LOG_TAG,item.toString());
                        }
                    }
                }
        );
    }

    private String newBoardChar() {
        int random = randomGen.nextInt(numchars);
        int i = random * charwidth;
        return glyphs.substring(i, i + charwidth);
    }

    private static void stopAnimation() {
        if (myTimerTask != null) {
            myTimerTask.cancel();
            myTimerTask = null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        int[] result = getScreenSize(this);
        int screenWidth = result[0];
        int screenHeight = result[1];
        int w = screenWidth * 4 / 5;
        int h = screenHeight * 2 / 3;
        updateLayout(w, h);
    }

    private static class LoadGlyphs extends AsyncTask<Void, Integer, Integer> {
        // https://stackoverflow.com/questions/44309241/warning-this-asynctask-class-should-be-static-or-leaks-might-occur
        private final WeakReference<LoadingActivity> activityReference;
        private final WeakReference<ProgressBar> progressBarReference;

        // only retain a weak reference to the activity
        LoadGlyphs(LoadingActivity context, ProgressBar progressBar) {
            activityReference = new WeakReference<>(context);
            progressBarReference = new WeakReference<>(progressBar);
            Log.d(LOG_TAG, "load glyphs: start");
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            LoadingActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) {
                return 0;
            }
            Application app = activity.getApplication();
            DataRepository repository = new DataRepository(app);
            for (String name : utf16glyphs) {
                Log.d(LOG_TAG, "utf16 font: " + name);
            }
            CharDao charDao = repository.getCharDao();
            int counter = 0;
            for (Map.Entry<String, String> entry : Glyphs.glyphs.entrySet()) {
                String name = entry.getKey();
                String glyphs = entry.getValue();

                int length = glyphs.length();
                if (utf16glyphs.contains(name)) {
                    Log.d(LOG_TAG, "length=" + length);
                    int[] result = getUnicodeStartEnd(glyphs, 2);
                    int start = result[0];
                    int end = result[1];
                    String startS = codepointToString(start);
                    String endS = codepointToString(end);
                    Log.d(LOG_TAG, String.format("utf16 %s length=%d start=%x end=%x %s %s ", name, length, start, end, startS, endS));

                    int cnt = charDao.getCount(start, end);
                    Log.d(LOG_TAG, "current count in db: " + cnt + " numchars=" + length / 2);
                    if (charDao.getCount(start, end) != length / 2) {
                        for (int i = 0; i < length / 2; i++) {
                            int id = stringToCodepoint(glyphs.substring(i * 2, (i + 1) * 2));
                            charDao.insert(new CharData(id, 0));
                        }
                        Log.d(LOG_TAG, "inserted " + length / 2 + " characters for " + name);
                    }
                    publishProgress(++counter);
                } else {
                    Log.d(LOG_TAG, "loading " + name);
                    int[] result = getUnicodeStartEnd(glyphs, 1);
                    int start = result[0];
                    int end = result[1];

                    int cnt = charDao.getCount(start, end);
                    Log.d(LOG_TAG, "current count in db: " + cnt + " numchars=" + length);

                    if (charDao.getCount(start, end) != length) {
                        for (int i = 0; i < length; i++) {
                            int id = (int) glyphs.charAt(i);
                            charDao.insert(new CharData(id, 0));
                        }
                        Log.d(LOG_TAG, "inserted " + length + " characters for " + name);
                    }
                    publishProgress(++counter);
                }
            }
            return counter;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int cnt = values[0];
            //Log.d(LOG_TAG, "progress update: cnt=" + cnt);
            ProgressBar progressBar = progressBarReference.get();
            if (progressBar == null) return;
            progressBar.setProgress(cnt);
        }

        @Override
        protected void onPostExecute(Integer cnt) {
            //super.onPostExecute(integer);
            Log.d(LOG_TAG, "loaded glyphs: " + cnt);
            ProgressBar progressBar = progressBarReference.get();
            if (progressBar == null) return;
            progressBar.setProgress(cnt);
            LoadingActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;
            Intent intent = new Intent(activity.getApplicationContext(), MainActivity.class);
            stopAnimation();
            activity.startActivity(intent);
            activity.finish();
        }
    }
}
