package org.catness.shapishgame.utils;

import android.graphics.Typeface;
import android.util.Log;

import static org.catness.shapishgame.utils.Constants.DEFAULT_FONT_SIZE;

public class SymbolSet {
    private static final String LOG_TAG = SymbolSet.class.getSimpleName();
    public final String glyphs;
    public final String fontname;
    public final String title;
    public Typeface typeface;
    public int charwidth = 1;
    public int fontsize = DEFAULT_FONT_SIZE;
    public int hpad = 0, vpad = 0;

    public SymbolSet(String glyphs, String fontname, String title) {
        Log.d(LOG_TAG, "title :" + title + " " + glyphs);
        this.glyphs = glyphs;
        this.fontname = fontname;
        this.title = title;
        this.typeface = null;
    }

    public void setCharWidth(int charwidth) {
        this.charwidth = charwidth;
    }

    public void setPadding(int hpad, int vpad) {
        this.hpad = hpad;
        this.vpad = vpad;
    }

    public void setFontsize(int fontsize) {
        this.fontsize = fontsize;
    }

}
