package org.catness.shapishgame.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static org.catness.shapishgame.utils.Constants.DEFAULT_FONT;

/*
Get the list of glyphs.
https://en.wikipedia.org/w/index.php?title=Category:Unicode_blocks
copy/paste the table with the codes to a file.

Cleanup the file to remove the code numbers and spaces, leaving only the characters: use the script unicode_block_list.py
 unicode_block_list.py - to create glyph strings

*/

public class Symbols {
    private static final String LOG_TAG = Symbols.class.getSimpleName();
    private final HashMap<String, SymbolSet> symbolSets;
    private final AssetManager am;

    public Symbols(final Context context) {
        am = context.getApplicationContext().getAssets();
        symbolSets = new HashMap<>();

        for (Map.Entry<String, String> entry : Glyphs.glyphs.entrySet()) {
            String name = entry.getKey();
            String glyphs = entry.getValue();
            SymbolSet tmp;
            switch (name) {
                case "Symbols":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Symbola.ttf", name)));
                    tmp.setPadding(2, 4);
                    break;
                case "Pictographs":
                case "Alchemical":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Symbola.ttf", name)));
                    tmp.setCharWidth(2);
                    tmp.setPadding(0, 4);
                    break;
                case "Devanagari":
                    // symbolSets.put(name, new SymbolSet(glyphs, "Lohit-Devanagari.ttf", title));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansDevanagari-Regular.ttf", name)));
                    tmp.setFontsize(30);
                    break;

                case "Egyptian":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NewGardinerSMP.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansEgyptianHieroglyphs-Regular.ttf", name)));
                    tmp.setCharWidth(2);
                    tmp.setFontsize(30);
                    tmp.setPadding(4, 8);
                    break;
                case "Hiragana":
                case "Katakana":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "rounded-mgenplus-2cp-heavy.ttf", name)));
                    tmp.setPadding(4, 0);
                    break;
                case "Armenian":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansArmenian-Regular.ttf", name)));
                    tmp.setPadding(2, 10);
                    break;
                case "Georgian":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansGeorgian-Regular.ttf", name)));
                    tmp.setPadding(2, 10);
                    break;
                case "Glagolitic":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansGlagolitic-Regular.ttf", name)));
                    tmp.setPadding(0, 10);
                    break;
                case "Vai":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Dukor.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansVai-Regular.ttf", name)));
                    tmp.setFontsize(40);
                    tmp.setPadding(2, 8);
                    break;
                case "Hangul":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Sunflower-Bold.ttf", name)));
                    tmp.setPadding(10, 16);
                    break;
                case "Taiviet":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "TaiHeritagePro-Bold.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansTaiViet-Regular.ttf", name)));
                    tmp.setFontsize(25);
                    break;
                case "Ethiopic":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "hiwua.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansEthiopic-Regular.ttf", name)));
                    tmp.setPadding(0, 10);
                    break;
                case "Mongolian":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansMongolian-Regular.ttf", name)));
                    tmp.setPadding(0, 4);
                    break;
                case "Tibetan":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansTibetan-Bold.ttf", name)));
                    tmp.setFontsize(25);
                    break;
                case "Javanese":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansJavanese-Regular.ttf", name)));
                    tmp.setFontsize(25);
                    break;
                case "Balinese":
                    //symbolSets.put(name, (tmp = new SymbolSet(glyphs, "AksaraBali.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansBalinese-Regular.ttf", name)));
                    tmp.setFontsize(25);
                    tmp.setPadding(0, 4);
                    break;

                case "Anatolian":
                    //symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Anatolian.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansAnatolianHieroglyphs-Regular.ttf", name)));
                    tmp.setCharWidth(2);
                    //tmp.setFontsize(45);
                    tmp.setPadding(0, 5);
                    break;
                case "Cuineform":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "CuneiformComposite.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansCuneiform-Regular.ttf", name)));
                    tmp.setCharWidth(2);
                    tmp.setPadding(0, 10);
                    tmp.setFontsize(20);
                    break;

                case "Runic":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "BabelStoneRunic.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "NotoSansRunic-Regular.ttf", name)));
                    tmp.setFontsize(45);
                    tmp.setPadding(0, 15);
                    break;
                case "Mahjong":
                    // symbolSets.put(name, (tmp = new SymbolSet(glyphs, "BabelStoneHan.ttf", title)));
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Symbola.ttf", name)));
                    tmp.setCharWidth(2);
                    tmp.setPadding(0, 10);
                    break;
                case "Playing cards":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Symbola.ttf", name)));
                    tmp.setCharWidth(2);
                    tmp.setFontsize(40);
                    break;
                case "Chess":
                    symbolSets.put(name, (tmp = new SymbolSet(glyphs, "Symbola.ttf", name)));
                    tmp.setCharWidth(2);
                    tmp.setPadding(0, 10);
                    break;

                default:
                    Log.d(LOG_TAG, "Error: font name not found: " + name);
                    break;
            }
        }
    }

    public Typeface getTypeface(String key) {
        Log.d(LOG_TAG, "get typeface: " + key);
        if (!symbolSets.containsKey(key)) {
            key = DEFAULT_FONT;
        }
        SymbolSet selected = symbolSets.get(key);
        if (selected.typeface == null) {
            selected.typeface = Typeface.createFromAsset(am, String.format(Locale.ENGLISH, "fonts/%s", selected.fontname));
        }
        return selected.typeface;
    }

    public SymbolSet select(String key) {
        Log.d(LOG_TAG, "select: " + key);
        if (!symbolSets.containsKey(key)) {
            key = DEFAULT_FONT;
            Log.d(LOG_TAG, "Not found, falling back to " + key);
        }
        SymbolSet selected = symbolSets.get(key);
        if (selected.typeface == null) {
            selected.typeface = Typeface.createFromAsset(am, String.format(Locale.ENGLISH, "fonts/%s", selected.fontname));
            Log.d(LOG_TAG, "create typeface from " + selected.fontname);
        }
        return selected;
    }

}
