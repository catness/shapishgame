package org.catness.shapishgame.utils;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import org.catness.shapishgame.R;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

/**
 * Created by catness on 5/24/19.
 */

public class Utils {
    private static final String LOG_TAG = Utils.class.getSimpleName();
    public static final Random randomGen = new Random();

    public static String colorHex(int color) {
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return String.format(Locale.ENGLISH, "#%02X%02X%02X%02X", a, r, g, b);
    }


    public static String codepointToString(int cp) {
        StringBuilder sb = new StringBuilder();
        if (Character.isBmpCodePoint(cp)) {
            sb.append((char) cp);
        } else if (Character.isValidCodePoint(cp)) {
            sb.append(Character.highSurrogate(cp));
            sb.append(Character.lowSurrogate(cp));
        } else {
            sb.append('?');
        }
        return sb.toString();
    }

    public static int stringToCodepoint(String s) {
        int w = s.length();

        if (w == 1) {
            return (int) s.charAt(0);
        } else { // currently only for w == 2
            // https://hajsoftutorial.com/java-supplementary-characters-utf-16-encoding/
            return Character.toCodePoint(s.charAt(0), s.charAt(1));
        }
    }

    public static int[] getUnicodeStartEnd(String glyphs, int charsize) {
        int[] result = new int[2];
        int length = glyphs.length();
        if (charsize == 1) {
            result[0] = (int) glyphs.charAt(0);
            result[1] = (int) glyphs.charAt(length - 1);
        } else {
            result[0] = stringToCodepoint(glyphs.substring(0, 2));
            result[1] = stringToCodepoint(glyphs.substring(length - 2, length));
        }
        return result;
    }


    public static List<Integer> pickRandom(List<Integer> lst, int n) {
        List<Integer> copy = new LinkedList<>(lst);
        Collections.shuffle(copy);
        return copy.subList(0, n);
    }

    public static int getRandom(int min, int max) {
        return randomGen.nextInt((max - min) + 1) + min;
    }

    public static int[] getScreenSize(AppCompatActivity context) {
        int[] result = new int[2];
        DisplayMetrics metrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int screenWidth = metrics.widthPixels;
        int screenHeight = metrics.heightPixels;
        Log.d(LOG_TAG, "screen : " + screenWidth + ", " + screenHeight);

        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true)) {
            int heightNavbar = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
            Log.d(LOG_TAG, "navbar: " + heightNavbar);
            screenHeight -= heightNavbar;
        }

        int resid = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resid > 0) {
            int heightStatusBar = context.getResources().getDimensionPixelSize(resid);
            Log.d(LOG_TAG, "status bar: " + heightStatusBar);
            screenHeight -= heightStatusBar;
        }
        Log.d(LOG_TAG, "final screenheight : " + screenHeight);
        result[0] = screenWidth;
        result[1] = screenHeight;
        return result;
    }

}
