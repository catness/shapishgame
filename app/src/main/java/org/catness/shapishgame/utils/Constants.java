package org.catness.shapishgame.utils;

import android.graphics.Color;

import java.util.Arrays;
import java.util.List;

public class Constants {
    public static final int INPUT_COLS = 5;
    public static final int TIMERBAR_MAX = 1000;
    public static final int POWERUP_BAR_MAX = 100;
    public static final int MAX_LIVES = 5;
    public static final int MAX_PENALTY = 10;
    public static final String COLOR_STATUS_LIFE = "#20FF20";
    public static final String COLOR_STATUS_GAMEOVER = "#e0e000";

    public static final String COLOR_STARS_BG = "#606060";
    private static final String COLOR_STARS_SILVER = "#d0d0d0";
    private static final String COLOR_STARS_GOLD = "#FFD700";
    private static final String COLOR_STARS_RUBY = "#e0115f";
    private static final String COLOR_STARS_SAPPHIRE = "#0f52ba";
    private static final String COLOR_STARS_AQUAMARINE = "#7ebed7";
    private static final String COLOR_STARS_AMETHYST = "#9966cc";
    private static final String COLOR_STARS_EMERALD = "#50c878";

    public static final int colorBG = Color.parseColor("#000000");
    public static final int starsColorSilver = Color.parseColor(COLOR_STARS_SILVER);
    public static final int starsColorGold = Color.parseColor(COLOR_STARS_GOLD);
    public static final int starsColorRuby = Color.parseColor(COLOR_STARS_RUBY);
    public static final int starsColorSapphire = Color.parseColor(COLOR_STARS_SAPPHIRE);
    public static final int starsColorAquamarine = Color.parseColor(COLOR_STARS_AQUAMARINE);
    public static final int starsColorAmethyst = Color.parseColor(COLOR_STARS_AMETHYST);
    public static final int starsColorEmerald = Color.parseColor(COLOR_STARS_EMERALD);

    public static final List<Integer> levelsColors = Arrays.asList(starsColorSilver, starsColorGold,
            starsColorRuby, starsColorSapphire, starsColorAquamarine, starsColorAmethyst, starsColorEmerald);


    public static final int SCORE_BAR_MAX = 200;
    public static final int SCORE_BAR_MAX_DEBUG = 10;
    public static final int MAX_KEYS = 8;   // on the status panel

    public static final int MAX_STARS = 5;  // in the dex
    //public static final int MAX_AMOUNT_IN_DB = levelsColors.size()*MAX_STARS+1; // 1: simply appears in the dex; 2: appears + 1 star; etc
    public static final int MAX_AMOUNT_IN_DB = 36; // must be constant for charDao

    public static final int ANIM_DURATION = 15000;
    public static final int ANIM_DURATION_MIN = 5000;
    public static final int ANIM_SPEEDUP = 100;
    public static final float SPEED_BONUS = 0.5f;
    public static final int SLOW_SPEED_COEFF = 3;
    public static final int SLOW_SPEED_COEFF_DEBUG = 20;

    public static final int DEFAULT_FONT_SIZE = 35;
    public static final String DEFAULT_FONT = "Symbols";
    public static final String DEFAULT_GRADIENT_1 = "#005cff";
    public static final String DEFAULT_GRADIENT_2 = "#00edff";

    public static final String PREF_BOARD_DEBUG = "pref_board_debug";
    public static final String PREF_DEX_DEBUG = "pref_dex_debug";
    public static final String PREF_HINT = "pref_hint";
    public static final String PREF_SOUND = "pref_sound";
    public static final String PREF_FONTS = "pref_fonts";
    public static final String PREF_SLOW = "pref_slow";
    public static final String PREF_GRADIENT_1 = "pref_gradient_1";
    public static final String PREF_GRADIENT_2 = "pref_gradient_2";
    public static final String PREF_SAMPLE_TEXT = "pref_sample_text";
    public static final String PREF_QUICK_WIN = "pref_quick_win";

    public static final int POWERUP_BONUS = 1;
    public static final int POWERUP_SPEED = 2;
    public static final int POWERUP_LIFE = 3;
    public static final int POWERUP_HINT = 4;
    public static final int POWERUP_RESET = 5;
    public static final int POWERUP_PAUSE = 6;

    public static final String DATABASE_NAME = "shapish";

    public static final String UNICODE_URL = "https://graphemica.com/";
}
