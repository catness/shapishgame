package org.catness.shapishgame;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.GridLayout;

import org.catness.shapishgame.elements.BoardItem;
import org.catness.shapishgame.utils.SymbolSet;

import java.util.ArrayList;
import java.util.Locale;

import static org.catness.shapishgame.utils.Constants.colorBG;
import static org.catness.shapishgame.utils.Utils.randomGen;
import static org.catness.shapishgame.utils.Utils.stringToCodepoint;

public class BoardPanel {
    private static final String LOG_TAG = BoardPanel.class.getSimpleName();
    private final ArrayList<BoardItem> items;
    public final ArrayList<String> chars;
    private final GridLayout gridLayout;
    private final int boardRows = 5;
    private final int boardColumns = 5;
    private boolean debug;
    private int charID = 0;
    private String glyphs;
    private int charwidth, numchars;
    private float cellSizeLimit = 0;
    private InputPanel inputPanel;
    private final Drawable hintBG;

    public BoardPanel(MainActivity context, SymbolSet selected, boolean debug, String color1, String color2) {
        this.glyphs = selected.glyphs;
        this.debug = debug;
        this.charwidth = selected.charwidth;
        numchars = glyphs.length() / charwidth;
        Log.d(LOG_TAG, "Board init: chars=" + numchars + "    " + glyphs);

        items = new ArrayList<>();
        chars = new ArrayList<>();
        gridLayout = context.findViewById(R.id.layout_grid);
        // Log.d(LOG_TAG, "********************************selected: " + selected.title);
        for (int i = 0; i < boardRows * boardColumns; i++) {
            BoardItem t = new BoardItem(context, null, R.attr.charStyle, i, this);
            t.setTypeface(selected.typeface);
            t.setTextSize(TypedValue.COMPLEX_UNIT_SP, selected.fontsize);
            t.setPadding(selected.hpad, selected.vpad, selected.hpad, selected.vpad);
            t.setColors(color1, color2);
            gridLayout.addView(t);
            items.add(t);
        }

        Resources res = context.getResources();
        hintBG = res.getDrawable(R.drawable.circle, null);

    }

    public void setInputPanel(InputPanel inputPanel) {
        this.inputPanel = inputPanel;
    }

    public void checkSelected(int id) {
        inputPanel.checkSelected(id);
    }

    public void setDebug(boolean isDebug) {
        debug = isDebug;
    }

    public void updateLayout(int width, int height) {
        ViewGroup.LayoutParams params = gridLayout.getLayoutParams();
        params.width = width;
        params.height = height;
        Log.d(LOG_TAG, "size of the grid :" + params.width + ", " + params.height);
        gridLayout.setLayoutParams(params);
        gridLayout.setColumnCount(boardColumns);
        gridLayout.requestLayout();
        float limit = cellSizeLimit;
        int paddingGrid = gridLayout.getPaddingLeft();
        int paddingChar = items.get(0).getPaddingLeft();
        // I don't understand why padding has to be divided by 2, not multiplied????? something to do with display density?
        cellSizeLimit = (width / boardColumns) - paddingGrid / 2; // substract the padding

        //https://stackoverflow.com/questions/10016343/gridlayout-not-gridview-how-to-stretch-all-children-evenly
        //https://stackoverflow.com/a/15341447 - 20 is some standard margin
        //cellSizeLimit = (int) ((width-20*boardColumns)/boardColumns);

        Log.d(LOG_TAG, "current padding : " + paddingGrid + "," + paddingChar +
                " cellSizeLimit=" + cellSizeLimit);
        for (int i = 0; i < boardRows * boardColumns; i++) {
            items.get(i).setCellSizeLimit(cellSizeLimit);
            items.get(i).setMinWidth((int) cellSizeLimit);
            items.get(i).setWidth((int) cellSizeLimit);
        }

        if (inputPanel != null) inputPanel.updateLayout(cellSizeLimit);
        if (limit == 0f) reset(); // call reset only the 1st time on resize

    }

    public void resetFont(SymbolSet selected) {
        for (int i = 0; i < boardRows * boardColumns; i++) {
            BoardItem t = items.get(i);
            t.setTypeface(selected.typeface);
            t.setTextSize(TypedValue.COMPLEX_UNIT_SP, selected.fontsize);
            t.setPadding(selected.hpad, selected.vpad, selected.hpad, selected.vpad);
        }

        this.glyphs = selected.glyphs;
        this.charwidth = selected.charwidth;
        numchars = glyphs.length() / charwidth;
        charID = 0;
    }

    public void resetColors(String color1, String color2) {
        for (int i = 0; i < boardRows * boardColumns; i++) {
            items.get(i).setColors(color1, color2);
        }
    }

    public void reset() {
        Log.d(LOG_TAG, "reset! charID = " + charID);
        chars.clear();
        int id = 0;
        String s;
        for (int row = 0; row < boardRows; row++) {
            for (int col = 0; col < boardColumns; col++) {
                //items.get(id).clearActions();
                //items.get(id).addAction(alpha(1f));
                if (debug) {
                    s = newBoardChar(charID);
                    if (++charID >= glyphs.length() / charwidth) {
                        Log.d(LOG_TAG, "\n\n\n\n\n\n*************************** END OF FONT : " + glyphs.length() / charwidth);
                        charID = 0;
                    }
                } else {
                    s = newBoardChar();
                }
                chars.add(s);
                items.get(id).updateFast(s);
                highlight(id, false);
                id++;
            }
        }
        //Log.d(LOG_TAG, "\n\n\nBOARD:  " + toString());
        inputPanel.reset();
    }

    public void backDebug() {
        for (int i = 0; i < boardRows * boardColumns * 2; i++) {
            if (--charID < 0) charID = glyphs.length() / charwidth - 1;
        }
        // Log.d(LOG_TAG, "Back! now charID=" + charID);
        reset();
    }

    public void highlight(int num, boolean on) {
        if (on) {
            items.get(num).setBackgroundDrawable(hintBG);
        } else {
            items.get(num).setBackgroundColor(colorBG);
        }
    }

    public void resetHighlight() {
        for (int i = 0; i < boardRows * boardColumns; i++) {
            highlight(i, false);
        }
    }

    public int randomChar() {
        return randomGen.nextInt(chars.size());
    }

    private String newBoardChar() {
        String s;
        do {
            int random = randomGen.nextInt(numchars);
            int i = random * charwidth;
            s = glyphs.substring(i, i + charwidth);
            //Log.d(LOG_TAG, "~~~~~~board char : numchars=" + numchars + " i=" +i + " " + s);
        } while (chars.contains(s));
        return s;
    }

    private String newBoardChar(int id) {
        // Log.d(LOG_TAG, "~~~~~~board char : numchars=" + numchars + " id=" +id + " " + s);
        return glyphs.substring(id * charwidth, (id + 1) * charwidth);
    }

    public void update(int id) {
        String s = newBoardChar();
        items.get(id).update(s);
        chars.set(id, s);
        //int codepoint = stringToCodepoint(s);
        //Log.d(LOG_TAG,"NEW BOARD CHAR : " + String.format(Locale.ENGLISH,"%d: %s (%x) ",id,s,codepoint));
        //Log.d(LOG_TAG,"Board now : " + this.toString());
    }

    @Override
    public String toString() {

        String out;
        int cnt = 0;
        StringBuilder outBuilder = new StringBuilder("\n");
        for (int i = 0; i < chars.size(); i++) {
            int codepoint = stringToCodepoint(chars.get(i));
            String c = String.format(Locale.ENGLISH, "   %d: %s %x   ", i, chars.get(i), codepoint);
            cnt++;
            if (cnt >= boardColumns) {
                c += "\n";
                cnt = 0;
            }
            outBuilder.append(c);
        }
        out = outBuilder.toString();
        out += "\nsize=" + chars.size() + "\n";
        int id = 0;
        StringBuilder outBuilder1 = new StringBuilder(out);
        for (int row = 0; row < boardRows; row++) {
            for (int col = 0; col < boardColumns; col++) {
                outBuilder1.append(items.get(id++).toString()).append("  ");
            }
        }
        out = outBuilder1.toString();
        return out;
    }
}
