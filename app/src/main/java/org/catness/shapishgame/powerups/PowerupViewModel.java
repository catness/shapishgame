package org.catness.shapishgame.powerups;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import org.catness.shapishgame.database.DataRepository;

import java.util.List;

public class PowerupViewModel extends AndroidViewModel {
    private final DataRepository repository;

    public PowerupViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
    }

    public LiveData<List<PowerupData>> getAllPowerups() {
        return repository.getAllPowerups();
    }

    public void update(int id, int amount) {
        repository.updatePowerup(id, amount);
    }

    public void decrement(int id) {
        repository.decrementPowerup(id);
    }

    public void increment(int id) {
        repository.incrementPowerup(id);
    }


}
