package org.catness.shapishgame.powerups;

import android.graphics.Typeface;
import android.util.Log;

import org.catness.shapishgame.BoardPanel;
import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;

import static org.catness.shapishgame.utils.Constants.POWERUP_RESET;


public class PowerupReset extends Powerup {
    private static final String LOG_TAG = PowerupReset.class.getSimpleName();
    private final BoardPanel boardPanel;

    public PowerupReset(PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, InputPanel inputPanel, BoardPanel boardPanel) {
        super(POWERUP_RESET, powerupViewModel, context, typeface, new String(Character.toChars(0x1f503)), 500, inputPanel);
        this.boardPanel = boardPanel;
    }

    public void start() {
        boardPanel.reset();
    }

    public void end() {
    }

}