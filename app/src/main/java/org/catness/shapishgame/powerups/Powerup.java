package org.catness.shapishgame.powerups;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;
import org.catness.shapishgame.elements.ProgressBarAnimation;
import org.catness.shapishgame.R;

import java.util.Locale;

import static org.catness.shapishgame.utils.Constants.POWERUP_BAR_MAX;
import static org.catness.shapishgame.utils.Constants.POWERUP_PAUSE;

/**
 * Created by catness on 5/25/19.
 */

public class Powerup {
    private static final String LOG_TAG = Powerup.class.getSimpleName();
    private final MainActivity context;
    private final RelativeLayout layout;
    private final ProgressBar progressBar;
    private final ImageView powerupBG;
    public final View view;
    private ProgressBarAnimation anim;
    private boolean isActive = false;
    private int counter = 0;
    private final int animDuration;
    protected final InputPanel inputPanel;
    private final int id; // powerup id from the database (POWERUP_ in constants)
    private final PowerupViewModel powerupViewModel;

    public Powerup(final int id, final PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, String sym, int animDuration, final InputPanel inputPanel) {
        Log.d(LOG_TAG, "create powerup: id=" + id);
        this.id = id;
        this.powerupViewModel = powerupViewModel;
        this.context = context;
        this.animDuration = animDuration;
        this.inputPanel = inputPanel;
        view = context.getLayoutInflater().inflate(R.layout.powerup, null);
        layout = view.findViewById(R.id.powerupLayout);
        progressBar = view.findViewById(R.id.powerupProgress);
        TextView progressSym = view.findViewById(R.id.powerupText);
        powerupBG = view.findViewById(R.id.powerupBG);

        //https://demonuts.com/circular-progress-bar/

        Resources res = context.getResources();
        Drawable drawable = res.getDrawable(R.drawable.circular, null);
        progressBar.setProgress(POWERUP_BAR_MAX);   // Main Progress
        progressBar.setSecondaryProgress(POWERUP_BAR_MAX); // Secondary Progress
        progressBar.setMax(POWERUP_BAR_MAX); // Maximum Progress
        progressBar.setProgressDrawable(drawable);
        progressSym.setTypeface(typeface);
        progressSym.setText(sym);
        progressSym.setTextColor(Color.parseColor("#000000"));

        updateSymColor();
        init();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (inputPanel.isGameOver) return;
                if (!isActive && counter > 0) {
                    if (id != 0 && id != POWERUP_PAUSE) {
                        counter--;
                        updateSymColor();
                        powerupViewModel.decrement(id);
                    }
                    isActive = true;
                    progressBar.startAnimation(anim);
                    start();
                }
            }
        });
    }

    public void start() {

    }

    public void end() {

    }


    public void updateLayout(int width, int height, int num, int id) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();

        int margin = 15;
        params.width = width;
        params.height = (height - margin * 2) / num;
        if (id == 0) {
            params.setMargins(0, margin, 0, 0);
        } else if (id == num - 1) {
            params.setMargins(0, 0, 0, margin);
        } else {
            params.setMargins(0, 0, 0, 0);
        }

        // Log.d(LOG_TAG, "~~~~~~~~~~~~~~~~~~~~~~~~ Powerup w,h=" + params.width + "," + params.height);
        layout.setLayoutParams(params);
        layout.invalidate();
        layout.requestLayout();

        float scale = 1.5f;
        progressBar.setScaleX(scale);
        progressBar.setScaleY(scale);

        int size = width / 2;
        Bitmap bMap = BitmapFactory.decodeResource(context.getResources(), R.drawable.whitecircle);
        Bitmap bMapScaled = Bitmap.createScaledBitmap(bMap, size, size, true);
        powerupBG.setImageBitmap(bMapScaled);

    }

    private void init() {
        anim = new ProgressBarAnimation(progressBar, 0, POWERUP_BAR_MAX);
        anim.setDuration(animDuration);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                isActive = false;
                end();
            }
        });
    }

    private void updateSymColor() {
        String color;
        if (counter == 0) color = "#c0c0c0";
        else if (counter >= 50) color = "#00ff00";
        else {
            int i = (50 - counter) * 254 / 49 - 10;
            if (i < 0) i = 0;
            color = String.format(Locale.ENGLISH, "#%02X%02X%02X", i, 255, i);
        }
        // Log.d(LOG_TAG, "counter=" + counter + " color=" + color);
        powerupBG.setColorFilter(Color.parseColor(color));
    }

    public void add() {
        counter++;
        powerupBG.setColorFilter(Color.parseColor("#FFFF00"));
        if (id != 0) powerupViewModel.increment(id);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updateSymColor();
            }
        }, 500);
    }

    public void update(int amount) {
        if (counter == amount) return;
        counter = amount;
        if (id != 0) powerupViewModel.update(id, amount);
        updateSymColor();
    }

    public String toString() {
        return "Powerup id=" + id + " amount=" + counter + " animDuration=" + animDuration;
    }

}
