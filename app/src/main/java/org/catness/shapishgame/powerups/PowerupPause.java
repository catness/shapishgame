package org.catness.shapishgame.powerups;

import android.graphics.Typeface;
import android.util.Log;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;
import org.catness.shapishgame.StatusPanel;

import static org.catness.shapishgame.utils.Constants.POWERUP_PAUSE;

/**
 * Created by catness on 6/7/19.
 */

public class PowerupPause extends Powerup {
    private static final String LOG_TAG = PowerupPause.class.getSimpleName();
    private final StatusPanel statusPanel;

    public PowerupPause(PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, InputPanel inputPanel, StatusPanel statusPanel) {
        super(POWERUP_PAUSE, powerupViewModel, context, typeface, new String(Character.toChars(0x1f375)), 100, inputPanel);
        this.statusPanel = statusPanel;
    }

    public void start() {
        if (inputPanel.isPaused()) inputPanel.resume();
        else {
            statusPanel.removeKey();
            inputPanel.pause();
        }
    }

    public void end() {
    }
}
