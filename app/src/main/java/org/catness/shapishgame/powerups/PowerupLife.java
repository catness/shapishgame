package org.catness.shapishgame.powerups;

import android.graphics.Typeface;
import android.util.Log;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;
import org.catness.shapishgame.StatusPanel;

import static org.catness.shapishgame.utils.Constants.POWERUP_LIFE;

public class PowerupLife extends Powerup {
    private static final String LOG_TAG = PowerupLife.class.getSimpleName();
    private final StatusPanel statusPanel;

    public PowerupLife(PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, InputPanel inputPanel, StatusPanel statusPanel) {
        super(POWERUP_LIFE, powerupViewModel, context, typeface, new String(Character.toChars(0x1f5a4)), 500, inputPanel);
        this.statusPanel = statusPanel;
    }

    public void start() {
        statusPanel.addLife();
    }

    public void end() {}
}
