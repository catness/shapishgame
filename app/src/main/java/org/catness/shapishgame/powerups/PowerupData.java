package org.catness.shapishgame.powerups;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "powerups")
public class PowerupData {
    private final static String LOG_TAG = PowerupData.class.getSimpleName();
    @PrimaryKey
    public int id;
    public int amount;

    public PowerupData(int id, int amount) {
        this.id = id;
        this.amount = amount;
    }

    public String toString() {
        return "Powerup: id=" + id + " amount=" + amount;
    }


}
