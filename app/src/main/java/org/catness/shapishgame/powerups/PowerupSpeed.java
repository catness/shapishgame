package org.catness.shapishgame.powerups;


import android.graphics.Typeface;
import android.util.Log;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;

import static org.catness.shapishgame.utils.Constants.POWERUP_SPEED;

public class PowerupSpeed extends Powerup {
    private static final String LOG_TAG = PowerupSpeed.class.getSimpleName();

    public PowerupSpeed(PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, InputPanel inputPanel) {
        super(POWERUP_SPEED, powerupViewModel, context, typeface, new String(Character.toChars(0x1f563)), 15000, inputPanel);
    }

    public void start() {
        inputPanel.setSlowSpeed(true);
    }

    public void end() {
        inputPanel.setSlowSpeed(false);
    }

}
