package org.catness.shapishgame.powerups;

import android.graphics.Typeface;
import android.util.Log;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;

import static org.catness.shapishgame.utils.Constants.POWERUP_HINT;

public class PowerupHint extends Powerup {
    private static final String LOG_TAG = PowerupHint.class.getSimpleName();

    public PowerupHint(PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, InputPanel inputPanel) {
        super(POWERUP_HINT, powerupViewModel, context, typeface, new String(Character.toChars(0x1f4a1)), 5000, inputPanel);
    }

    public void start() {
        inputPanel.setHint(true);
    }

    public void end() {
        inputPanel.setHint(false);
    }

}
