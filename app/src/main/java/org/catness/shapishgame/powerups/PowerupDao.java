package org.catness.shapishgame.powerups;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PowerupDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(PowerupData powerup);

    @Query("select * from powerups")
    LiveData<List<PowerupData>> getAllPowerups();

    @Query("update powerups set amount=:amount where id=:id")
    void update(int id, int amount);

    @Query("update powerups set amount=amount-1 where id=:id")
    void decrement(int id);

    @Query("update powerups set amount=amount+1 where id=:id")
    void increment(int id);

    @Query("select count(*) from powerups")
    int getCount();

    @Query("DELETE FROM powerups")
    void deleteAll();

}
