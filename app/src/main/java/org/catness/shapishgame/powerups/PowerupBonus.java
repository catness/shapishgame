package org.catness.shapishgame.powerups;

import android.graphics.Typeface;
import android.util.Log;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;
import org.catness.shapishgame.StatusPanel;

import static org.catness.shapishgame.utils.Constants.POWERUP_BONUS;


public class PowerupBonus extends Powerup {
    private static final String LOG_TAG = PowerupBonus.class.getSimpleName();
    private final StatusPanel statusPanel;

    public PowerupBonus(PowerupViewModel powerupViewModel, MainActivity context, Typeface typeface, InputPanel inputPanel, StatusPanel statusPanel) {
        super(POWERUP_BONUS, powerupViewModel, context, typeface, new String(Character.toChars(0x1f320)), 20000, inputPanel);
        this.statusPanel = statusPanel;
    }

    public void start() {
        statusPanel.setBonusScore(true);
    }

    public void end() {
        statusPanel.setBonusScore(false);
    }

}