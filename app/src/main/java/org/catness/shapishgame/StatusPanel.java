package org.catness.shapishgame;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Math.min;
import static org.catness.shapishgame.utils.Constants.COLOR_STARS_BG;
import static org.catness.shapishgame.utils.Constants.COLOR_STATUS_GAMEOVER;
import static org.catness.shapishgame.utils.Constants.COLOR_STATUS_LIFE;
import static org.catness.shapishgame.utils.Constants.MAX_KEYS;
import static org.catness.shapishgame.utils.Constants.MAX_LIVES;
import static org.catness.shapishgame.utils.Constants.MAX_PENALTY;
import static org.catness.shapishgame.utils.Constants.SCORE_BAR_MAX;
import static org.catness.shapishgame.utils.Constants.SCORE_BAR_MAX_DEBUG;
import static org.catness.shapishgame.utils.Constants.levelsColors;
import static org.catness.shapishgame.utils.Constants.starsColorAmethyst;
import static org.catness.shapishgame.utils.Constants.starsColorAquamarine;
import static org.catness.shapishgame.utils.Constants.starsColorEmerald;
import static org.catness.shapishgame.utils.Constants.starsColorGold;
import static org.catness.shapishgame.utils.Constants.starsColorRuby;
import static org.catness.shapishgame.utils.Constants.starsColorSapphire;
import static org.catness.shapishgame.utils.Constants.starsColorSilver;
import static org.catness.shapishgame.utils.Utils.randomGen;

public class StatusPanel {
    private static final String LOG_TAG = StatusPanel.class.getSimpleName();
    private final MainActivity context;
    private final TextView livesView, starsView;
    private final ProgressBar scoreView;
    private int livesNum;
    private float score = 0;
    private int scorePenalty = 1;
    private boolean isBonusScore = false;
    private static final String heart = new String(Character.toChars(0x1f5a4));
    private static final String gameover = new String(Character.toChars(0x1f494));
    private static final String keyicon = new String(Character.toChars(0x1f511));
    private static final int heartColor = Color.parseColor(COLOR_STATUS_LIFE);
    private static final int gameoverColor = Color.parseColor(COLOR_STATUS_GAMEOVER);
    private static final int starsColorBG = Color.parseColor(COLOR_STARS_BG);
    private float speedBonus = 0;
    private int scoreBarMax = SCORE_BAR_MAX;
    private final int charwidth = 2; // width of the Pictograms typeface
    private final Spannable spannable;
    public int numkeys = 0;
    private TimerTask myTimerTask = null;
    private final ArrayList<Integer> colors = new ArrayList<>(Arrays.asList(heartColor, gameoverColor,
            starsColorSilver, starsColorGold,
            starsColorRuby, starsColorSapphire, starsColorAquamarine, starsColorAmethyst, starsColorEmerald));


    public StatusPanel(MainActivity context, Typeface typeface) {
        this.context = context;
        livesView = context.findViewById(R.id.livesView);
        livesView.setTypeface(typeface);
        starsView = context.findViewById(R.id.starsView);
        starsView.setTypeface(typeface);
        StringBuilder keysBuilder = new StringBuilder();
        for (int i = 0; i < MAX_KEYS; i++) {
            keysBuilder.append(keyicon);
        }
        String keys = keysBuilder.toString();
        starsView.setText(keys);
        starsView.setTextColor(starsColorBG);
        spannable = new SpannableString(keys);

        scoreView = context.findViewById(R.id.scoreView);
        scoreView.setProgress(0);
        scoreView.setMax(scoreBarMax);
        reset();
    }

    public void setQuickWin(boolean isQuickWin) {
        scoreBarMax = (isQuickWin) ? SCORE_BAR_MAX_DEBUG : SCORE_BAR_MAX;
        scoreView.setMax(scoreBarMax);
    }


    public void reset() {
        stopAnimation();
        livesNum = MAX_LIVES;
        updateLives(livesNum);
        score = 0;
        scorePenalty = 1;
        numkeys = 0;
        spannable.setSpan(new ForegroundColorSpan(starsColorBG), 0, MAX_KEYS * charwidth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        starsView.setText(spannable);
        showScore(score);
    }

    public void addLife() {
        livesNum++;
        updateLives(livesNum);
    }

    public boolean loseLife() {
        if (livesNum > 0) {
            livesNum--;
            updateLives(livesNum);
        }
        return (livesNum <= 0); // return true if gameover
    }

    private void updateLives(int livesNum) {
        String s;
        if (livesNum == 0) {
            s = gameover;
            livesView.setTextColor(gameoverColor);
        } else {
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < min(livesNum, MAX_LIVES + 2); i++) {
                sBuilder.append(heart);
            }
            s = sBuilder.toString();
            livesView.setTextColor(heartColor);
        }
        livesView.setText(s);
    }

    public void startAnimation() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < MAX_LIVES; i++) {
            s.append(heart);
        }
        livesView.setText(s.toString());

        new Timer().scheduleAtFixedRate(myTimerTask = new TimerTask() {
            @Override
            public void run() {
                animateColors();
            }

        }, 0, 400);//put here time 1000 milliseconds=1 second
    }

    private void stopAnimation() {
        if (myTimerTask != null) {
            myTimerTask.cancel();
            myTimerTask = null;
        }
    }

    private void animateColors() {
        context.runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        int color = colors.get(randomGen.nextInt(colors.size()));
                        livesView.setTextColor(color);
                        color = colors.get(randomGen.nextInt(colors.size()));
                        spannable.setSpan(new ForegroundColorSpan(color), 0, MAX_KEYS * charwidth, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        starsView.setText(spannable);
                    }
                }
        );
    }


    private void showScore(float scoreNum) {
        int num = (int) (scoreNum / scoreBarMax);
        int progress = (int) scoreNum % scoreBarMax;
        scoreView.setProgress(progress);
        if (num == numkeys) {
            return;
        }
        numkeys = num;
        if (numkeys > MAX_KEYS * levelsColors.size()) numkeys = MAX_KEYS * levelsColors.size();
        starsView.setText(getKeys(numkeys));
    }

    private Spannable getKeys(int amount) {
        if (amount <= 0) {
            spannable.setSpan(new ForegroundColorSpan(starsColorBG), 0, MAX_KEYS * charwidth, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            int fgcolor = 0;
            for (int i = 0; i < levelsColors.size(); i++) {
                //Log.d(LOG_TAG, "i=" + i + " amount=" + amount);
                if (amount <= MAX_KEYS) {
                    fgcolor = levelsColors.get(i);
                    break;
                }
                amount -= MAX_KEYS;
            }
            if (fgcolor == 0) {
                amount = MAX_KEYS;
                fgcolor = levelsColors.get(levelsColors.size() - 1);
            }
            spannable.setSpan(new ForegroundColorSpan(fgcolor), 0, amount * charwidth, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(starsColorBG), amount * charwidth, MAX_KEYS * charwidth, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return spannable;
    }

    public void setBonusScore(boolean isBonusScore) {
        this.isBonusScore = isBonusScore;
    }

    public void updateScore(int inputCols, int amount) {
        float s;
        if (inputCols <= 2) {
            s = 1;
        } else if (inputCols <= 4) {
            s = 2;
        } else {
            s = 3;
        }
        if (amount > 0) {
            s += scoreAddon(amount);
        }

        s += speedBonus;
        if (isBonusScore) s *= 2;
        score += s;
        //Log.d(LOG_TAG, "amount=" + amount + " s=" + s + " score=" + score);
        showScore(score);
    }

    public boolean isMaxKeys() {
        return (numkeys >= MAX_KEYS * levelsColors.size());
    }

    private float scoreAddon(int amount) {
        if (amount <= 5) return amount / 2;
        amount -= 5;
        if (amount <= 5) return 5 / 2 + amount / 3;
        amount -= 5;
        if (amount <= 5) return 5 / 2 + 5 / 3 + amount / 4;
        amount -= 5;
        if (amount <= 5) return 5 / 2 + 5 / 3 + 5 / 4 + amount / 5;
        amount -= 5;
        return 5 / 2 + 5 / 3 + 5 / 4 + 1 + amount / 6;
    }

    public void removeKey() {
        score -= scoreBarMax;
        if (score < 0) score = 0;
        showScore(score);
    }

    public void decreaseScore() {
        score -= scorePenalty;
        if (score < 0) score = 0;
        scorePenalty++;
        if (scorePenalty > MAX_PENALTY) scorePenalty = MAX_PENALTY;
        showScore(score);
    }

    public void updateSpeedBonus(float speedBonus) {
        this.speedBonus = speedBonus;
    }
}
