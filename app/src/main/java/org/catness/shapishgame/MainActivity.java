package org.catness.shapishgame;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;

import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.chars.CharViewModel;
import org.catness.shapishgame.utils.SymbolSet;
import org.catness.shapishgame.utils.Symbols;

import java.util.List;

import static org.catness.shapishgame.utils.Constants.DEFAULT_FONT;
import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_2;
import static org.catness.shapishgame.utils.Constants.PREF_BOARD_DEBUG;
import static org.catness.shapishgame.utils.Constants.PREF_FONTS;
import static org.catness.shapishgame.utils.Constants.PREF_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.PREF_GRADIENT_2;
import static org.catness.shapishgame.utils.Constants.PREF_HINT;
import static org.catness.shapishgame.utils.Constants.PREF_QUICK_WIN;
import static org.catness.shapishgame.utils.Constants.PREF_SLOW;
import static org.catness.shapishgame.utils.Constants.PREF_SOUND;
import static org.catness.shapishgame.utils.Utils.colorHex;
import static org.catness.shapishgame.utils.Utils.getScreenSize;
import static org.catness.shapishgame.utils.Utils.getUnicodeStartEnd;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private BoardPanel boardPanel;
    public InputPanel inputPanel;
    private ToolsPanel toolsPanel;
    private StatusPanel statusPanel;
    private Symbols symbols;
    private SymbolSet selected;
    private boolean isDebug = false;
    private boolean isHint = false;
    private boolean isSlow = false;
    private boolean isSound = true;
    private boolean isQuickWin = false;
    private String gradient1;
    private String gradient2;
    private String currentFont = DEFAULT_FONT;
    private CharViewModel charViewModel;
    private int start = 0, end = 0;
    private LiveData<List<CharData>> charData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        symbols = new Symbols(this);
        Typeface typefacePictograms = symbols.getTypeface("Pictograms");

        initPreferences();

        statusPanel = new StatusPanel(this, typefacePictograms);
        boardPanel = new BoardPanel(this, selected, isDebug, gradient1, gradient2);
        inputPanel = new InputPanel(this, selected, boardPanel, statusPanel, gradient1, gradient2);
        boardPanel.setInputPanel(inputPanel);
        toolsPanel = new ToolsPanel(this, typefacePictograms, statusPanel, boardPanel, inputPanel);

        inputPanel.setHint(isHint);
        inputPanel.setSlowDebug(isSlow);
        inputPanel.setToolsPanel(toolsPanel);
        inputPanel.setSound(isSound);
        statusPanel.setQuickWin(isQuickWin);

        charViewModel = ViewModelProviders.of(this).get(CharViewModel.class);
        updateCharData();
    }

    private void updateCharData() {
        if (start != 0 && end != 0) {
            charData.removeObservers(this);
        }
        int result[] = getUnicodeStartEnd(selected.glyphs, selected.charwidth);
        start = result[0];
        end = result[1];
        charData = charViewModel.getAllChars(start, end);
        charData.observe(this, new Observer<List<CharData>>() {
            @Override
            public void onChanged(@Nullable List<CharData> charData) {
                inputPanel.setCharData(charData);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        int[] result = getScreenSize(this);
        int screenWidth = result[0];
        int screenHeight = result[1];
        int w = screenWidth * 4 / 5;
        int h = screenHeight * 2 / 3;
        boardPanel.updateLayout(w, h);
        toolsPanel.updateLayout(screenWidth - w, h);
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                boardPanel.reset();
                return true;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                boardPanel.backDebug();
                return true;
            case KeyEvent.KEYCODE_P:
                inputPanel.pause();
                return true;
            case KeyEvent.KEYCODE_R:
                inputPanel.resume();
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    public void restart() {
        inputPanel.restart();
        statusPanel.reset();
        boardPanel.reset();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_settings:
                intent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_dex:
                intent = new Intent(MainActivity.this, DexActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        isSound = sharedPreferences.getBoolean(PREF_SOUND, true);
        isDebug = sharedPreferences.getBoolean(PREF_BOARD_DEBUG, false);
        isQuickWin = sharedPreferences.getBoolean(PREF_QUICK_WIN, false);
        isHint = sharedPreferences.getBoolean(PREF_HINT, false);
        isSlow = sharedPreferences.getBoolean(PREF_SLOW, false);
        currentFont = sharedPreferences.getString(PREF_FONTS, DEFAULT_FONT);
        selected = symbols.select(currentFont);
        gradient1 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_1, Color.parseColor(DEFAULT_GRADIENT_1)));
        gradient2 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_2, Color.parseColor(DEFAULT_GRADIENT_2)));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        // Log.d(LOG_TAG, "\n\n\nshared pref changed key=" + key + "=");
        switch (key) {
            case PREF_SOUND:
                isSound = sharedPreferences.getBoolean(key, true);
                inputPanel.setSound(isSound);
                break;

            case PREF_BOARD_DEBUG:
                isDebug = sharedPreferences.getBoolean(key, false);
                boardPanel.setDebug(isDebug);
                restart();
                break;

            case PREF_QUICK_WIN:
                isQuickWin = sharedPreferences.getBoolean(key, false);
                statusPanel.setQuickWin(isQuickWin);
                break;

            case PREF_HINT:
                isHint = sharedPreferences.getBoolean(key, false);
                inputPanel.setHint(isHint);
                restart();
                break;

            case PREF_SLOW:
                isSlow = sharedPreferences.getBoolean(key, false);
                inputPanel.setSlowDebug(isSlow);
                restart();
                break;


            case PREF_GRADIENT_1:
            case PREF_GRADIENT_2:
                gradient1 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_1, Color.parseColor(DEFAULT_GRADIENT_1)));
                gradient2 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_2, Color.parseColor(DEFAULT_GRADIENT_2)));
                boardPanel.resetColors(gradient1, gradient2);
                inputPanel.resetColors(gradient1, gradient2);
                break;

            case PREF_FONTS:
                currentFont = sharedPreferences.getString(PREF_FONTS, DEFAULT_FONT);
                selected = symbols.select(currentFont);
                updateCharData();
                boardPanel.resetFont(selected);
                inputPanel.resetFont(selected);
                restart();
                break;
        }
    }
}
