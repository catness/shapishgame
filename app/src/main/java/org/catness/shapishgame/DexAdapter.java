package org.catness.shapishgame;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.elements.GradientTextView;
import org.catness.shapishgame.utils.SymbolSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.catness.shapishgame.utils.Constants.COLOR_STARS_BG;
import static org.catness.shapishgame.utils.Constants.MAX_STARS;
import static org.catness.shapishgame.utils.Constants.levelsColors;
import static org.catness.shapishgame.utils.Utils.codepointToString;

// https://www.raywenderlich.com/995-android-gridview-tutorial

public class DexAdapter extends BaseAdapter {
    private static final String LOG_TAG = DexAdapter.class.getSimpleName();
    private final DexActivity mContext;
    public List<CharData> chars;
    private String color1, color2;
    private final int colorUnknown = Color.parseColor("#909090");
    private static final String star = new String(Character.toChars(0x2605));
    private static final int starsColorBG = Color.parseColor(COLOR_STARS_BG);
    private final Spannable spannable;
    private boolean debug = false;
    private SymbolSet symbolSet;

    public DexAdapter(DexActivity context, SymbolSet symbolSet, String color1, String color2) {
        this.mContext = context;
        this.symbolSet = symbolSet;
        this.color1 = color1;
        this.color2 = color2;
        chars = new ArrayList<>();
        StringBuilder stars = new StringBuilder();
        for (int i = 0; i < MAX_STARS; i++) {
            stars.append(star);
        }
        spannable = new SpannableString(stars.toString());
    }

    public void setChars(List<CharData> chars) {
        //Log.d(LOG_TAG, "setChars : number=" + chars.size());
        this.chars = chars;
        notifyDataSetChanged();
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
        notifyDataSetChanged();

    }

    public void setColors(String color1, String color2) {
        this.color1 = color1;
        this.color2 = color2;
        //Log.d(LOG_TAG, "change colors: " + color1 + " " + color2);
        notifyDataSetChanged();
    }

    public void setSymbolSet(SymbolSet symbolSet) {
        this.symbolSet = symbolSet;
    }

    @Override
    public int getCount() {
        return chars.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (chars.size() == 0) return null;
        final CharData chardata = chars.get(position);
        //Log.d(LOG_TAG, "getView: position=" + position + " char=" + chardata.toString());
        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.dex_item, null);
        }

        final TextView unicodeView = convertView.findViewById(R.id.unicode);
        String code = String.format("U+%04X", chardata.id);
        unicodeView.setText(code);

        final GradientTextView glyphView = convertView.findViewById(R.id.glyph);
        final TextView amountView = convertView.findViewById(R.id.amount);

        glyphView.setTypeface(symbolSet.typeface);
        glyphView.setPadding(0, symbolSet.vpad, 0, 2);
        if (chardata.amount > 0) {
            glyphView.setColors(color1, color2);
            amountView.setText(getStars(chardata.amount));
            String glyph = codepointToString(chardata.id);
            glyphView.setText(glyph);
            amountView.setVisibility(View.VISIBLE);
        } else {
            glyphView.setText("?");
            glyphView.setColors(colorUnknown, colorUnknown);
            amountView.setVisibility(View.INVISIBLE);
        }

        RelativeLayout debugButtons = convertView.findViewById(R.id.debug_buttons);
        if (debug) {
            debugButtons.setVisibility(View.VISIBLE);
            final TextView minusView = convertView.findViewById(R.id.minus);
            final TextView plusView = convertView.findViewById(R.id.plus);
            minusView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(LOG_TAG, String.format(Locale.ENGLISH, " Decrement char: %X", chardata.id));
                    mContext.decrementChar(chardata.id);
                }
            });
            plusView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(LOG_TAG, String.format(Locale.ENGLISH, " Increment char: %X", chardata.id));
                    mContext.incrementChar(chardata.id);
                }
            });
        } else {
            debugButtons.setVisibility(View.GONE);
        }

        return convertView;
    }

    private Spannable getStars(int amount) {
        amount -= 1; // we don't draw stars for amount=1 (it's simply allowing to display the characters).
        int charwidth = 1;
        if (amount <= 0) {
            spannable.setSpan(new ForegroundColorSpan(starsColorBG), 0, MAX_STARS * charwidth, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            int fgcolor = 0;
            for (int i = 0; i < levelsColors.size(); i++) {
                // Log.d(LOG_TAG, "i=" + i + " amount=" + amount);
                if (amount <= MAX_STARS) {
                    fgcolor = levelsColors.get(i);
                    break;
                }
                amount -= MAX_STARS;
            }
            if (fgcolor == 0) {
                amount = MAX_STARS;
                fgcolor = levelsColors.get(levelsColors.size() - 1);
            }
            // Log.d(LOG_TAG, "final amount=" + amount);
            spannable.setSpan(new ForegroundColorSpan(fgcolor), 0, amount * charwidth, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(starsColorBG), amount * charwidth, MAX_STARS * charwidth, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return spannable;
    }
}
