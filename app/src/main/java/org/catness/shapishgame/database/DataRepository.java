package org.catness.shapishgame.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import org.catness.shapishgame.chars.CharDao;
import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.powerups.PowerupDao;
import org.catness.shapishgame.powerups.PowerupData;

import java.util.List;

public class DataRepository {
    private static final String LOG_TAG = DataRepository.class.getSimpleName();
    private final PowerupDao powerupDao;
    private final CharDao charDao;

    public DataRepository(Application application) {
        ShapishDatabase db = ShapishDatabase.getDatabase(application);
        powerupDao = db.powerupDao();
        charDao = db.charDao();
    }

    public CharDao getCharDao() {
        return charDao;
    }

    public LiveData<List<PowerupData>> getAllPowerups() {
        return powerupDao.getAllPowerups();
    }

    public void updatePowerup(int id, int amount) {
        new updatePowerupAsyncTask(powerupDao).execute(id, amount);
    }

    public void incrementPowerup(int id) {
        new incrementPowerupAsyncTask(powerupDao).execute(id);
    }

    public void decrementPowerup(int id) {
        new decrementPowerupAsyncTask(powerupDao).execute(id);
    }

    public void deleteAllPowerups() {
        new deleteAllPowerupsAsyncTask(powerupDao).execute();
    }


    public LiveData<List<CharData>> getAllChars(int start, int end) {
        return charDao.getCharData(start, end);
    }

    public List<Integer> getChars(int start, int end, int num) {
        return charDao.getChars(start, end, num);
    }

    public Integer getCharsCount(int start, int end, int num) {
        return charDao.getCharsCount(start, end, num);
    }

    public void incrementChar(int id) {
        charDao.increment(id);
    }

    public void incrementCharAsync(int id) {
        new incrementCharAsyncTask(charDao).execute(id);
    }

    public void decrementCharAsync(int id) {
        new decrementCharAsyncTask(charDao).execute(id);
    }


    private static class updatePowerupAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final PowerupDao mAsyncTaskDao;

        updatePowerupAsyncTask(PowerupDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            int id = params[0];
            int amount = params[1];
            // Log.d(LOG_TAG, " update powerup : " + id + " " + amount);
            mAsyncTaskDao.update(id, amount);
            return null;
        }
    }

    private static class decrementPowerupAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final PowerupDao mAsyncTaskDao;

        decrementPowerupAsyncTask(PowerupDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.decrement(params[0]);
            return null;
        }
    }

    private static class incrementPowerupAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final PowerupDao mAsyncTaskDao;

        incrementPowerupAsyncTask(PowerupDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.increment(params[0]);
            return null;
        }
    }

    private static class deleteAllPowerupsAsyncTask extends AsyncTask<Void, Void, Void> {
        private final PowerupDao mAsyncTaskDao;

        deleteAllPowerupsAsyncTask(PowerupDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Log.d(LOG_TAG, "Deleting all powerups...");
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    private static class incrementCharAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final CharDao mAsyncTaskDao;

        incrementCharAsyncTask(CharDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.increment(params[0]);
            return null;
        }
    }

    private static class decrementCharAsyncTask extends AsyncTask<Integer, Void, Void> {
        private final CharDao mAsyncTaskDao;

        decrementCharAsyncTask(CharDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Integer... params) {
            mAsyncTaskDao.decrement(params[0]);
            return null;
        }
    }

}
