package org.catness.shapishgame.database;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import org.catness.shapishgame.MainActivity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import static org.catness.shapishgame.utils.Constants.MAX_AMOUNT_IN_DB;
import static org.catness.shapishgame.utils.Utils.codepointToString;
import static org.catness.shapishgame.utils.Utils.pickRandom;

/**
 * Created by catness on 5/31/19.
 */

public class AddToDex extends AsyncTask<Void, Void, List<Integer>> {
    private static final String LOG_TAG = AddToDex.class.getSimpleName();

    private final WeakReference<MainActivity> activityReference;
    private final int start, end, num;
    private DataRepository repository;

    public AddToDex(MainActivity context, int start, int end, int num) {
        this.activityReference = new WeakReference<>(context);
        this.start = start;
        this.end = end;
        this.num = num;
    }

    @Override
    protected List<Integer> doInBackground(Void... voids) {
        MainActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return null;
        Application app = activity.getApplication();
        repository = new DataRepository(app);
        List<Integer> random = select();
        if (random == null) return null;
        for (Integer codepoint : random) {
            Log.d(LOG_TAG, String.format("Picked: %X %s", codepoint, codepointToString(codepoint)));
            repository.incrementChar(codepoint);
        }
        return random;
    }

    @Override
    protected void onPostExecute(List<Integer> chars) {
        MainActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return;
        String out = "";
        if (chars != null) {
            StringBuilder outBuilder = new StringBuilder();
            for (Integer mychar : chars) {
                outBuilder.append(codepointToString(mychar)).append(" ");
            }
            out = outBuilder.toString();
            Log.d(LOG_TAG, "------------------ " + out);
            //Toast.makeText(activity,out, Toast.LENGTH_LONG).show();
        }
        activity.inputPanel.setDialogGlyphs(out);
    }

    private List<Integer> select() {
        int[] thresholds = new int[]{2, 5, 10, 15, 20, 25, 30, MAX_AMOUNT_IN_DB - 1};
        int toPick = this.num;
        List<Integer> picked = new ArrayList<>();
        for (int threshold : thresholds) {
            int nchars = repository.getCharsCount(start, end, threshold);
            Log.d(LOG_TAG, "select : nchars=" + nchars + " threshold=" + threshold + " toPick=" + toPick);
            if (nchars == 0) continue;
            if (nchars < toPick) {
                List<Integer> chars = repository.getChars(start, end, threshold); // picked nchars, must pick more
                picked.addAll(chars);
                toPick -= nchars;
            } else {
                List<Integer> chars = repository.getChars(start, end, threshold); // picked nchars, must pick more
                List<Integer> random = pickRandom(chars, toPick);
                picked.addAll(random);
                break;
            }
        }
        if (picked.size() == 0) {
            Log.d(LOG_TAG, "All characters found");
            return null;
        }
        return picked;
    }

}
