package org.catness.shapishgame.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;

import org.catness.shapishgame.chars.CharDao;
import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.powerups.PowerupDao;
import org.catness.shapishgame.powerups.PowerupData;

import static org.catness.shapishgame.utils.Constants.DATABASE_NAME;
import static org.catness.shapishgame.utils.Constants.POWERUP_BONUS;
import static org.catness.shapishgame.utils.Constants.POWERUP_HINT;
import static org.catness.shapishgame.utils.Constants.POWERUP_LIFE;
import static org.catness.shapishgame.utils.Constants.POWERUP_PAUSE;
import static org.catness.shapishgame.utils.Constants.POWERUP_RESET;
import static org.catness.shapishgame.utils.Constants.POWERUP_SPEED;

@Database(entities = {PowerupData.class, CharData.class}, version = 2, exportSchema = false)
public abstract class ShapishDatabase extends RoomDatabase {
    private static final String LOG_TAG = ShapishDatabase.class.getSimpleName();

    public abstract PowerupDao powerupDao();

    public abstract CharDao charDao();

    private static ShapishDatabase INSTANCE;

    public static ShapishDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ShapishDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ShapishDatabase.class, DATABASE_NAME)
                            //.addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_1_4)
                            .fallbackToDestructiveMigration()
                            .addCallback(initialize)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static final RoomDatabase.Callback initialize =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new initializeAsync(INSTANCE).execute();
                }
            };

    /**
     * Populate the database in the background.
     */
    private static class initializeAsync extends AsyncTask<Void, Void, Void> {
        private final PowerupDao powerupDao;

        initializeAsync(ShapishDatabase db) {
            powerupDao = db.powerupDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
            //powerupDao.deleteAll();
            Log.d("Database", "................Initialize");
            if (powerupDao.getCount() == 0) {
                powerupDao.insert(new PowerupData(POWERUP_BONUS, 2));
                powerupDao.insert(new PowerupData(POWERUP_SPEED, 2));
                powerupDao.insert(new PowerupData(POWERUP_LIFE, 2));
                powerupDao.insert(new PowerupData(POWERUP_HINT, 2));
                powerupDao.insert(new PowerupData(POWERUP_RESET, 2));
                powerupDao.insert(new PowerupData(POWERUP_PAUSE, 20));
            }
            return null;
        }

    }


}
