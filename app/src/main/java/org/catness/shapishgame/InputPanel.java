package org.catness.shapishgame;

import android.media.AudioAttributes;
import android.media.SoundPool;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.database.AddToDex;
import org.catness.shapishgame.elements.GameOverDialog;
import org.catness.shapishgame.elements.InputItem;
import org.catness.shapishgame.elements.TimerBar;
import org.catness.shapishgame.utils.SymbolSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.view.View.GONE;
import static org.catness.shapishgame.utils.Constants.INPUT_COLS;
import static org.catness.shapishgame.utils.Utils.getUnicodeStartEnd;
import static org.catness.shapishgame.utils.Utils.stringToCodepoint;

public class InputPanel {
    private static final String LOG_TAG = InputPanel.class.getSimpleName();
    private final MainActivity context;
    private final ArrayList<InputItem> items;
    private final ArrayList<String> chars;
    private final TimerBar timerBar;
    private int inputCols;
    private final TextView messageView;
    private final BoardPanel board;
    private final StatusPanel statusPanel;
    private ToolsPanel toolsPanel;
    private boolean isPaused = false;
    public boolean isGameOver = false;
    private boolean isHint = false;
    private SymbolSet selected;
    private final Map<Integer, Integer> charAmounts;
    private final GameOverDialog gameOverDialog;
    private final int soundPositive;
    private final int soundNegative;
    private final int soundGameOverLose;
    private final int soundGameOverWin;
    private final int soundPowerup;
    private final SoundPool soundPool;
    private float volume = 1;
    private boolean isSound = true;

    public InputPanel(final MainActivity context, SymbolSet selected, final BoardPanel board, final StatusPanel statusPanel, String color1, String color2) {
        this.context = context;
        this.board = board;
        this.statusPanel = statusPanel;
        this.selected = selected;

        RelativeLayout inputLayout = context.findViewById(R.id.layout_input);
        timerBar = new TimerBar(context, this);
        timerBar.reset();

        messageView = context.findViewById(R.id.message);
        messageView.setVisibility(GONE);

        gameOverDialog = new GameOverDialog(context);

        // Load the sounds
        AudioAttributes audioAttrib = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_GAME)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        soundPool = new SoundPool.Builder()
                .setMaxStreams(5)
                .setAudioAttributes(audioAttrib)
                .build();

        soundPositive = soundPool.load(context, R.raw.positive, 1);
        soundNegative = soundPool.load(context, R.raw.negative, 1);
        soundGameOverLose = soundPool.load(context, R.raw.gameover_lose, 1);
        soundGameOverWin = soundPool.load(context, R.raw.gameover_win, 1);
        soundPowerup = soundPool.load(context, R.raw.powerup, 1);

        items = new ArrayList<>();
        chars = new ArrayList<>();
        charAmounts = new HashMap<>();

        inputCols = 1;
        for (int col = 0; col < INPUT_COLS; col++) {
            InputItem item = new InputItem(context, null, R.attr.charStyle);
            item.setTypeface(selected.typeface);
            item.setTextSize(TypedValue.COMPLEX_UNIT_SP, selected.fontsize);
            item.setPadding(selected.hpad, selected.vpad, selected.hpad, selected.vpad);
            item.setColors(color1, color2);
            items.add(item);
            item.setVisibility(View.INVISIBLE);
            int itemid = 100 + col;
            item.setId(itemid);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            if (col == 0) {
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            } else {
                params.addRule(RelativeLayout.RIGHT_OF, itemid - 1);
            }
            inputLayout.addView(item, params);
        }
        inputLayout.requestLayout();
        timerBar.init();
    }

    public void updateLayout(float cellSizeLimit) {
        for (int col = 0; col < INPUT_COLS; col++) {
            items.get(col).setCellSizeLimit(cellSizeLimit);
        }
    }

    public void setCharData(List<CharData> charData) {
        charAmounts.clear();
        for (CharData c : charData) {
            charAmounts.put(c.id, c.amount);
        }
    }

    public void resetFont(SymbolSet selected) {
        this.selected = selected;
        for (int col = 0; col < INPUT_COLS; col++) {
            items.get(col).setTypeface(selected.typeface);
            items.get(col).setTextSize(TypedValue.COMPLEX_UNIT_SP, selected.fontsize);
            items.get(col).setPadding(selected.hpad, selected.vpad, selected.hpad, selected.vpad);
        }
        inputCols = 1;
    }

    public void resetColors(String color1, String color2) {
        for (int col = 0; col < INPUT_COLS; col++) {
            items.get(col).setColors(color1, color2);
        }
    }

    public void setToolsPanel(ToolsPanel toolsPanel) {
        this.toolsPanel = toolsPanel;
    }

    public void setHint(boolean isHint) {

        this.isHint = isHint;
        if (isHint) {
            for (int i = 0; i < board.chars.size(); i++) {
                String c = board.chars.get(i);
                if (chars.contains(c)) {
                    board.highlight(i, true);
                }
            }
        } else {
            board.resetHighlight();
        }
    }

    public void setSlowSpeed(boolean isSlowSpeed) {
        timerBar.setSlowSpeed(isSlowSpeed);
    }

    public void setSlowDebug(boolean isSlowSpeed) {
        timerBar.setSlowSpeedDebug(isSlowSpeed);
    }


    public void restart() {
        messageView.setVisibility(GONE);
        timerBar.restart();
        isGameOver = false;
        inputCols = 1;
        statusPanel.updateSpeedBonus(0);
        Log.d(LOG_TAG, "Restarting...");
    }

    public void timerEnded() {
        if (!isPaused && !isGameOver) {
            if (statusPanel.loseLife()) gameover();
            else {
                inputCols = 1;
                reset();
            }
        }
    }

    public void updateSpeedBonus(float speedBonus) {
        // from timerBar
        statusPanel.updateSpeedBonus(speedBonus);
    }

    public void reset() {
        chars.clear();
        if (isHint) board.resetHighlight();
        int id = 0;
        for (int col = 0; col < INPUT_COLS; col++) {
            if (col < inputCols) {
                String s = newInputChar();
                chars.add(s);
                items.get(id).setVisibility(View.VISIBLE);
                items.get(id).updateFast(s);
            } else {
                items.get(id).setVisibility(View.INVISIBLE);
            }
            id++;
        }
        timerBar.speedUp();
        timerBar.reset();
        timerBar.activate();
    }

    private boolean update(int id) {
        chars.remove(id);
        int cnt = 0;
        for (String c : chars) {
            items.get(cnt++).setText(c);
        }
        items.get(cnt).setText("");
        boolean ret = chars.isEmpty();
        if (ret) {
            if (inputCols < INPUT_COLS) inputCols++;
        }
        return ret;
    }

    private String newInputChar() {
        String s;
        int num;

        do {
            num = board.randomChar();
            s = board.chars.get(num);
        } while (chars.contains(s));
        if (isHint) board.highlight(num, true);
        return s;
    }

    private int isFound(int id) {
        String s = board.chars.get(id);
        board.highlight(id, false);
        int found = chars.indexOf(s);
        if (found >= 0) {
            return found;
        } else {
            return -1;
        }
    }

    public void pause() {
        isPaused = true;
        timerBar.pause();
    }

    public void resume() {
        isPaused = false;
        timerBar.resume();
    }

    public boolean isPaused() {
        return this.isPaused;
    }

    private void gameover() {
        isGameOver = true;

        for (int col = 0; col < INPUT_COLS; col++) {
            items.get(col).setVisibility(View.INVISIBLE);
        }

        timerBar.hide();
        messageView.setVisibility(View.VISIBLE);
        messageView.setText(context.getString(R.string.game_over_title));
        Log.d(LOG_TAG, "GAME OVER stars=" + statusPanel.numkeys);
        if (statusPanel.numkeys > 0) {
            if (isSound) soundPool.play(soundGameOverWin, volume, volume, 1, 0, 1f);
            int result[] = getUnicodeStartEnd(selected.glyphs, selected.charwidth);
            AddToDex add = new AddToDex(context, result[0], result[1], statusPanel.numkeys);
            add.execute();
        } else {
            if (isSound) soundPool.play(soundGameOverLose, volume, volume, 1, 0, 1f);
        }
        gameOverDialog.show(statusPanel.numkeys, selected.typeface);
    }

    public void checkSelected(int id) {
        if (isGameOver) return;
        if (isPaused) resume();
        Log.d(LOG_TAG, "selected item: " + id);
        int found = isFound(id);
        if (found >= 0) {
            Log.d(LOG_TAG, " --- found ");
            if (isSound) soundPool.play(soundPositive, volume, volume, 1, 0, 1f);
            int codepoint = stringToCodepoint(board.chars.get(id));
            int amount = 0;
            if (charAmounts.containsKey(codepoint)) {
                amount = charAmounts.get(codepoint);
            }
            statusPanel.updateScore(inputCols, amount);
            boolean empty = update(found);
            if (empty) {
                if (statusPanel.isMaxKeys()) {
                    statusPanel.startAnimation();
                    gameover();
                    return;
                }
                board.reset();
                if (inputCols == INPUT_COLS) {
                    int powerup = toolsPanel.addPowerup();
                    if (powerup > 0 && isSound)
                        soundPool.play(soundPowerup, volume, volume, 1, 0, 1f);
                }
            } else {
                // Log.d(LOG_TAG, "updating the board... " + id);
                board.update(id);
            }
        } else {   // clicked wrong character
            if (isSound) soundPool.play(soundNegative, volume, volume, 1, 0, 1f);
            statusPanel.decreaseScore();
        }
    }

    public void setDialogGlyphs(String glyphs) {
        gameOverDialog.setGlyphs(glyphs, selected.typeface);
    }

    public void setSound(boolean isSound) {
        this.isSound = isSound;
    }

}
