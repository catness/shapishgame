package org.catness.shapishgame;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.BaseAdapter;

import com.rarepebble.colorpicker.ColorPreference;

import org.catness.shapishgame.elements.SamplePreference;
import org.catness.shapishgame.utils.Glyphs;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.catness.shapishgame.utils.Constants.DEFAULT_FONT;
import static org.catness.shapishgame.utils.Constants.PREF_FONTS;
import static org.catness.shapishgame.utils.Constants.PREF_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.PREF_GRADIENT_2;
import static org.catness.shapishgame.utils.Constants.PREF_SAMPLE_TEXT;
import static org.catness.shapishgame.utils.Utils.colorHex;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends AppCompatPreferenceActivity {
    private static final String LOG_TAG = SettingsActivity.class.getSimpleName();
    private static SamplePreference samplePreference;
    private static BaseAdapter adapter;
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static final Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference.setSummary(
                        index >= 0
                                ? listPreference.getEntries()[index]
                                : null);

            } else if (preference instanceof ColorPreference) {
                //Log.d(LOG_TAG, "got key " + preference.getKey());
                // int color = ((ColorPreference) preference).getColor();
                //Log.d(LOG_TAG, "current color " + colorHex(color));
                int newcolor = Integer.parseInt(stringValue);
                //Log.d(LOG_TAG, "new color " + colorHex(newcolor));
                ((ColorPreference) preference).setColor(newcolor);

                int c1 = PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getInt(PREF_GRADIENT_1, 0);
                int c2 = PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getInt(PREF_GRADIENT_2, 0);
                samplePreference.setColors(colorHex(c1), colorHex(c2));
                adapter.notifyDataSetChanged();

            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                int c1 = PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getInt(PREF_GRADIENT_1, 0);
                int c2 = PreferenceManager.getDefaultSharedPreferences(preference.getContext()).getInt(PREF_GRADIENT_2, 0);
                ((SamplePreference) preference).setColors(colorHex(c1), colorHex(c2));
                preference.setSummary(stringValue);
            }
            return true;
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Binds a preference's summary to its value. More specifically, when the
     * preference's value is changed, its summary (line of text below the
     * preference title) is updated to reflect the value. The summary is also
     * immediately updated upon calling this method. The exact display format is
     * dependent on the type of preference.
     *
     * @see #sBindPreferenceSummaryToValueListener
     */
    private static void bindPreferenceSummaryToValue(Preference preference) {
        // Set the listener to watch for value changes.
        preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

        // Trigger the listener immediately with the preference's
        // current value.

        //Log.d(LOG_TAG, "bindPreferenceSummarytovalue" + " " + preference.getKey());

        if (preference instanceof ColorPreference) {
            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getInt(preference.getKey(), 0));
        } else {

            sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                            .getDefaultSharedPreferences(preference.getContext())
                            .getString(preference.getKey(), ""));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();
    }


    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.pref_headers, target);
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    protected boolean isValidFragment(String fragmentName) {
        return PreferenceFragment.class.getName().equals(fragmentName)
                || GeneralPreferenceFragment.class.getName().equals(fragmentName)
                || DebugPreferenceFragment.class.getName().equals(fragmentName);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);
            setHasOptionsMenu(true);

            final ListPreference listPreference = (ListPreference) findPreference(PREF_FONTS);
            // THIS IS REQUIRED IF YOU DON'T HAVE 'entries' and 'entryValues' in your XML
            setFontsPreferenceData(listPreference);

            samplePreference = (SamplePreference) findPreference(PREF_SAMPLE_TEXT);

            PreferenceScreen root = getPreferenceScreen();
            adapter = (BaseAdapter) (root.getRootAdapter());

            bindPreferenceSummaryToValue(findPreference(PREF_FONTS));
            bindPreferenceSummaryToValue(findPreference(PREF_SAMPLE_TEXT));
            bindPreferenceSummaryToValue(findPreference(PREF_GRADIENT_1));
            bindPreferenceSummaryToValue(findPreference(PREF_GRADIENT_2));
        }

        private void setFontsPreferenceData(ListPreference lp) {
            // CharSequence[] entries =  { "Symbols", "Hiragana","Pictographs", "Mahjong" };
            // CharSequence[] entryValues = {"1" , "2"};
            List<String> listItems = new ArrayList<>();
            for (Map.Entry<String, String> entry : Glyphs.glyphs.entrySet()) {
                String name = entry.getKey();
                listItems.add(name);
            }
            final CharSequence[] entries = listItems.toArray(new CharSequence[listItems.size()]);

            lp.setEntries(entries);
            lp.setDefaultValue(DEFAULT_FONT);
            lp.setEntryValues(entries);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }

    /**
     * This fragment shows notification preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class DebugPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_debug);
            setHasOptionsMenu(true);

        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == android.R.id.home) {
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;
            }
            return super.onOptionsItemSelected(item);
        }
    }


}
