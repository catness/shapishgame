package org.catness.shapishgame.chars;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.Locale;

import static org.catness.shapishgame.utils.Utils.codepointToString;

@Entity(tableName = "chars")
public class CharData {
    @PrimaryKey
    public final int id;
    public final int amount;

    public CharData(int id, int amount) {
        this.id = id;
        this.amount = amount;
    }

    public String toString() {
        return String.format(Locale.ENGLISH, "Char: id=%X amount=%d %s", id, amount, codepointToString(id));
    }

}
