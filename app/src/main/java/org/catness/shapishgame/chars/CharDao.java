package org.catness.shapishgame.chars;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import static org.catness.shapishgame.utils.Constants.MAX_AMOUNT_IN_DB;

/**
 * Created by catness on 5/31/19.
 */

@Dao
public interface CharDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(CharData charData);

    @Query("select * from chars where id>=:start and id<=:end")
    LiveData<List<CharData>> getCharData(int start, int end);

    @Query("select count(*) from chars where id>=:start and id<=:end and amount<=:num")
    Integer getCharsCount(int start, int end, int num);

    @Query("select id from chars where id>=:start and id<=:end and amount<=:num")
    List<Integer> getChars(int start, int end, int num);

    @Query("update chars set amount=amount+1 where id=:id and amount<" + MAX_AMOUNT_IN_DB)
    void increment(int id);

    @Query("update chars set amount=amount-1 where id=:id and amount>0")
    void decrement(int id);

    @Query("select count(*) from chars where id>=:start and id<=:end")
    int getCount(int start, int end);

    @Query("DELETE FROM chars")
    void deleteAll();

}
