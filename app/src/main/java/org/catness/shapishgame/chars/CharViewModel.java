package org.catness.shapishgame.chars;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.database.DataRepository;

import java.util.List;

/**
 * Created by catness on 6/1/19.
 */

public class CharViewModel extends AndroidViewModel {
    private final DataRepository repository;

    public CharViewModel(@NonNull Application application) {
        super(application);
        repository = new DataRepository(application);
    }

    public LiveData<List<CharData>> getAllChars(int start, int end) {
        return repository.getAllChars(start, end);
    }

    public void increment(int id) {
        repository.incrementCharAsync(id);
    }

    public void decrement(int id) {
        repository.decrementCharAsync(id);
    }

}

