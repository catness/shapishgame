package org.catness.shapishgame;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.catness.shapishgame.powerups.Powerup;
import org.catness.shapishgame.powerups.PowerupBonus;
import org.catness.shapishgame.powerups.PowerupData;
import org.catness.shapishgame.powerups.PowerupHint;
import org.catness.shapishgame.powerups.PowerupLife;
import org.catness.shapishgame.powerups.PowerupPause;
import org.catness.shapishgame.powerups.PowerupReset;
import org.catness.shapishgame.powerups.PowerupSpeed;
import org.catness.shapishgame.powerups.PowerupViewModel;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import static org.catness.shapishgame.utils.Constants.POWERUP_BONUS;
import static org.catness.shapishgame.utils.Constants.POWERUP_HINT;
import static org.catness.shapishgame.utils.Constants.POWERUP_LIFE;
import static org.catness.shapishgame.utils.Constants.POWERUP_PAUSE;
import static org.catness.shapishgame.utils.Constants.POWERUP_RESET;
import static org.catness.shapishgame.utils.Constants.POWERUP_SPEED;

public class ToolsPanel {
    private static final String LOG_TAG = ToolsPanel.class.getSimpleName();
    private final LinearLayout powerupsLayout;
    private int height = 0;
    private final LinkedHashMap<Integer, Powerup> powerups;

    public ToolsPanel(final MainActivity context, Typeface typeface, StatusPanel statusPanel, BoardPanel boardPanel, InputPanel inputPanel) {
        powerupsLayout = context.findViewById(R.id.layout_powerups);
        PowerupViewModel powerupViewModel = ViewModelProviders.of(context).get(PowerupViewModel.class);

        powerups = new LinkedHashMap<>();
        powerups.put(POWERUP_BONUS, new PowerupBonus(powerupViewModel, context, typeface, inputPanel, statusPanel));
        powerups.put(POWERUP_SPEED, new PowerupSpeed(powerupViewModel, context, typeface, inputPanel));
        powerups.put(POWERUP_LIFE, new PowerupLife(powerupViewModel, context, typeface, inputPanel, statusPanel));
        powerups.put(POWERUP_HINT, new PowerupHint(powerupViewModel, context, typeface, inputPanel));
        powerups.put(POWERUP_RESET, new PowerupReset(powerupViewModel, context, typeface, inputPanel, boardPanel));
        powerups.put(POWERUP_PAUSE, new PowerupPause(powerupViewModel, context, typeface, inputPanel, statusPanel));

        for (Map.Entry<Integer, Powerup> entry : powerups.entrySet()) {
            Powerup p = entry.getValue();
            powerupsLayout.addView(p.view);
        }
        powerups.get(POWERUP_PAUSE).update(10); // unlimited pause

        // https://stackoverflow.com/questions/12068945/get-layout-height-and-width-at-run-time-android
        powerupsLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                // Preventing extra work because method will be called many times.
                if (height == (bottom - top)) return;
                height = (bottom - top);
                int width = (right - left);
                int cnt = 0;
                for (Map.Entry<Integer, Powerup> entry : powerups.entrySet()) {
                    Powerup p = entry.getValue();
                    p.updateLayout(width, height, powerups.size(), cnt++);
                }
            }
        });

        powerupViewModel.getAllPowerups().observe(context, new Observer<List<PowerupData>>() {
            @Override
            public void onChanged(@Nullable final List<PowerupData> powerupDataList) {
                if (powerupDataList == null) {
                    return;
                }
                for (Map.Entry<Integer, Powerup> entry : powerups.entrySet()) {
                    Powerup p = entry.getValue();
                    Log.d(LOG_TAG, p.toString());
                }
                for (PowerupData p : powerupDataList) {
                    int id = p.id;
                    int amount = p.amount;
                    powerups.get(id).update(amount);
                }
            }
        });
    }

    public int addPowerup() {
        int keyNum = ThreadLocalRandom.current().nextInt(0, powerups.keySet().toArray().length);
        int key = (int) powerups.keySet().toArray()[keyNum];
        if (key == POWERUP_PAUSE)
            return 0; // pause is unlimited; and we don't actually need to add powerup every time
        powerups.get(key).add();
        return key;
    }

    public void updateLayout(int width, int height) {
        ViewGroup.LayoutParams params = powerupsLayout.getLayoutParams();
        params.width = width;
        params.height = height;

        powerupsLayout.setLayoutParams(params);
        powerupsLayout.requestLayout();
    }

}
