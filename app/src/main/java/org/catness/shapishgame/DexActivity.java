package org.catness.shapishgame;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import org.catness.shapishgame.chars.CharData;
import org.catness.shapishgame.chars.CharViewModel;
import org.catness.shapishgame.utils.SymbolSet;
import org.catness.shapishgame.utils.Symbols;

import java.util.List;
import java.util.Locale;

import static org.catness.shapishgame.utils.Constants.DEFAULT_FONT;
import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_2;
import static org.catness.shapishgame.utils.Constants.PREF_DEX_DEBUG;
import static org.catness.shapishgame.utils.Constants.PREF_FONTS;
import static org.catness.shapishgame.utils.Constants.PREF_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.PREF_GRADIENT_2;
import static org.catness.shapishgame.utils.Constants.UNICODE_URL;
import static org.catness.shapishgame.utils.Utils.codepointToString;
import static org.catness.shapishgame.utils.Utils.colorHex;
import static org.catness.shapishgame.utils.Utils.getUnicodeStartEnd;

// https://www.raywenderlich.com/995-android-gridview-tutorial

public class DexActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String LOG_TAG = DexActivity.class.getSimpleName();
    private CharViewModel charViewModel;
    private DexAdapter dexAdapter;
    private SymbolSet selected;
    private int start = 0, end = 0;
    private String currentFont;
    private Symbols symbols;
    private TextView titleView;
    private LiveData<List<CharData>> charData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dex);
        titleView = findViewById(R.id.title);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
        currentFont = sharedPreferences.getString(PREF_FONTS, DEFAULT_FONT);
        String gradient1 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_1, Color.parseColor(DEFAULT_GRADIENT_1)));
        String gradient2 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_2, Color.parseColor(DEFAULT_GRADIENT_2)));
        boolean debug = sharedPreferences.getBoolean(PREF_DEX_DEBUG, false);

        symbols = new Symbols(this);
        selected = symbols.select(currentFont);

        GridView gridView = findViewById(R.id.gridview);
        dexAdapter = new DexAdapter(this, selected, gradient1, gradient2);

        dexAdapter.setDebug(debug);
        gridView.setAdapter(dexAdapter);

        charViewModel = ViewModelProviders.of(this).get(CharViewModel.class);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                CharData chardata = dexAdapter.chars.get(position);
                String glyph = codepointToString(chardata.id);
                Log.d(LOG_TAG, "========= selected: " + glyph);
                if (chardata.amount > 0) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(UNICODE_URL + glyph));
                    startActivity(browserIntent);
                }
            }
        });
        initFont();
    }

    private void initFont() {
        titleView.setText(currentFont);
        int result[] = getUnicodeStartEnd(selected.glyphs, selected.charwidth);
        if (start != 0 && end != 0) {
            Log.d(LOG_TAG, String.format(Locale.ENGLISH, "removing observers for: %X %X", start, end));
            charData.removeObservers(this);
            dexAdapter.setSymbolSet(selected);
        }
        start = result[0];
        end = result[1];
        charData = charViewModel.getAllChars(start, end);
        charData.observe(this, new Observer<List<CharData>>() {
            @Override
            public void onChanged(@Nullable final List<CharData> chars) {
                Log.d(LOG_TAG, String.format(Locale.ENGLISH, "observing %s: %X %X", currentFont, start, end));
                dexAdapter.setChars(chars);
            }
        });
    }


    public void incrementChar(int id) {
        charViewModel.increment(id);
    }

    public void decrementChar(int id) {
        charViewModel.decrement(id);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dex, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.action_settings:
                intent = new Intent(DexActivity.this, SettingsActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        switch (key) {
            case PREF_GRADIENT_1:
            case PREF_GRADIENT_2:
                String gradient1 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_1, Color.parseColor(DEFAULT_GRADIENT_1)));
                String gradient2 = colorHex(sharedPreferences.getInt(PREF_GRADIENT_2, Color.parseColor(DEFAULT_GRADIENT_2)));
                dexAdapter.setColors(gradient1, gradient2);
                break;

            case PREF_FONTS:
                currentFont = sharedPreferences.getString(PREF_FONTS, DEFAULT_FONT);
                selected = symbols.select(currentFont);
                initFont();
                break;

            case PREF_DEX_DEBUG:
                boolean debug = sharedPreferences.getBoolean(key, false);
                dexAdapter.setDebug(debug);
                break;
        }
    }
}
