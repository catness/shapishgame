package org.catness.shapishgame.elements;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.catness.shapishgame.MainActivity;
import org.catness.shapishgame.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static org.catness.shapishgame.utils.Constants.MAX_KEYS;
import static org.catness.shapishgame.utils.Constants.levelsColors;
import static org.catness.shapishgame.utils.Utils.randomGen;

public class GameOverDialog {
    private static final String LOG_TAG = GameOverDialog.class.getSimpleName();
    private final MainActivity context;
    private AlertDialog dialog = null;
    private final View alertLayout;
    private final TextView titleView;
    private final TextView messageView;
    private final TextView rewardView;
    private final HashMap<Integer, ArrayList<String>> texts;

    public GameOverDialog(final MainActivity context) {
        this.context = context;

        texts = new HashMap<>();
        texts.put(0, new ArrayList<>(Arrays.asList("Nice work!", "Well done!", "Good job!")));
        texts.put(1, new ArrayList<>(Arrays.asList("Very good!", "Great!", "Cool!")));
        texts.put(2, new ArrayList<>(Arrays.asList("Wonderful!", "Wow!", "Impressive!")));
        texts.put(3, new ArrayList<>(Arrays.asList("Marvelous!", "Awesome!", "Splendid!")));
        texts.put(4, new ArrayList<>(Arrays.asList("Glorious!", "Amazing!", "Incredible!")));
        texts.put(5, new ArrayList<>(Arrays.asList("Fantastic!", "Fabulous!", "Brilliant!")));
        texts.put(6, new ArrayList<>(Arrays.asList("Extraordinary!", "Spectacular!", "Magnificent!")));

        LayoutInflater inflater = context.getLayoutInflater();
        alertLayout = inflater.inflate(R.layout.gameover, (ViewGroup) null);

        titleView = alertLayout.findViewById(R.id.gameover_title);
        messageView = alertLayout.findViewById(R.id.gameover_message);
        rewardView = alertLayout.findViewById(R.id.gameover_rewards);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(alertLayout);
        builder.setCancelable(true);
        dialog = builder.create();

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                context.restart();
            }
        });

    }

    public void show(int numkeys, Typeface typeface) {
        if (numkeys >= MAX_KEYS * levelsColors.size()) {
            titleView.setText(R.string.game_over_title_win);
        } else if (numkeys > 0) {
            titleView.setText(R.string.game_over_title);
        } else {
            titleView.setText(R.string.game_over_title_lose);
        }

        if (numkeys > 0) {
            messageView.setTypeface(typeface);
            int n = (numkeys - 1) / MAX_KEYS;
            String s;
            Log.d(LOG_TAG, "numkeys=" + numkeys + " n=" + n);
            if (texts.containsKey(n)) {
                s = texts.get(n).get(randomGen.nextInt(texts.get(n).size()));
                // Log.d(LOG_TAG, "random string: " + s);
            } else { // shouldn't happen but just in case
                s = texts.get(texts.size() - 1).get(0);
            }
            messageView.setText(context.getString(R.string.message_rewards, s));
        } else {
            messageView.setText("");
        }

        rewardView.setText("");
        dialog.show(); //show() should be called before dialog.getButton().
        Button ok = alertLayout.findViewById(R.id.gameover_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                context.restart();
            }
        });

    }

    public void setGlyphs(String glyphs, Typeface typeface) {
        if (dialog != null && dialog.isShowing()) {
            rewardView.setTypeface(typeface);
            if (!glyphs.equals("")) rewardView.setText(glyphs);
            else rewardView.setText(R.string.dex_complete);
        }
    }


}
