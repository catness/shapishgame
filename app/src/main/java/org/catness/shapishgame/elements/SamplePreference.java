package org.catness.shapishgame.elements;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;
import android.view.View;

import org.catness.shapishgame.R;

import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_2;

public class SamplePreference extends EditTextPreference {
    private final String LOG_TAG = SamplePreference.class.getSimpleName();
    private CharSequence summary;
    private String color1 = DEFAULT_GRADIENT_1, color2 = DEFAULT_GRADIENT_2;

    public SamplePreference(Context context) {
        super(context);
    }

    public SamplePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SamplePreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SamplePreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        GradientTextView sampleTextView = view.findViewById(R.id.sample_text_summary);
        sampleTextView.setColors(color1, color2);
        sampleTextView.setText(summary);
    }

    @Override
    public void setSummary(CharSequence summary) {
        //super.setSummary(summary);
        this.summary = summary;
    }

    public void setColors(String color1, String color2) {
        // Log.d(LOG_TAG, "set colors: " + color1 + " " + color2);
        this.color1 = color1;
        this.color2 = color2;
    }

}
