package org.catness.shapishgame.elements;

import android.content.Context;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.util.Log;

import java.util.Locale;

import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_1;
import static org.catness.shapishgame.utils.Constants.DEFAULT_GRADIENT_2;
import static org.catness.shapishgame.utils.Utils.colorHex;

// https://blog.stylingandroid.com/gradient-text/

public class GradientTextView extends android.support.v7.widget.AppCompatTextView {
    private static final String LOG_TAG = GradientTextView.class.getSimpleName();
    private LinearGradient gradient;
    private int color1 = Color.parseColor(DEFAULT_GRADIENT_1);
    private int color2 = Color.parseColor(DEFAULT_GRADIENT_2);


    public GradientTextView(Context context) {
        super(context, null, -1);
        initGradient();
    }

    public GradientTextView(Context context, AttributeSet attrs) {
        super(context, attrs, -1);
        initGradient();
    }

    public GradientTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initGradient();
    }

    public void setColors(int color1, int color2) {
        this.color1 = color1;
        this.color2 = color2;
        initGradient();
        getPaint().setShader(gradient);
    }


    public void setColors(String color1, String color2) {
        setColors(Color.parseColor(color1), Color.parseColor(color2));
    }

    private void initGradient() {
        gradient = new LinearGradient(
                0, 0, 0, getHeight(),
                color1, color2,
                Shader.TileMode.CLAMP);
    }

    public String toString() {
        return colorHex(color1)+":"+colorHex(color2);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            initGradient();
            getPaint().setShader(gradient);
        }
    }
}