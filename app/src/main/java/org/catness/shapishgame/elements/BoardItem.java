package org.catness.shapishgame.elements;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import org.catness.shapishgame.BoardPanel;
import org.catness.shapishgame.MainActivity;

import static org.catness.shapishgame.utils.Utils.stringToCodepoint;

/**
 * Created by catness on 5/18/19.
 */

public class BoardItem extends GradientTextView {
    private final int id;
    private static final String LOG_TAG = BoardItem.class.getSimpleName();
    private final MainActivity context;
    private final BoardPanel board;
    private float cellSizeLimit;

    public BoardItem(Context context, int id, BoardPanel board) {
        super(context);
        this.id = id;
        this.context = (MainActivity) context;
        this.board = board;
        init();
    }

    public BoardItem(Context context, AttributeSet attrs, int id, BoardPanel board) {
        super(context, attrs);
        this.id = id;
        this.context = (MainActivity) context;
        this.board = board;
        init();
    }

    public BoardItem(Context context, AttributeSet attrs, int defStyle, int id, BoardPanel board) {
        super(context, attrs, defStyle);
        this.id = id;
        this.context = (MainActivity) context;
        this.board = board;
        init();
    }

    private void init() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d(LOG_TAG, "clicked id=" + id);
                board.checkSelected(id);
            }
        });
    }

    public void setCellSizeLimit(float size) {
        cellSizeLimit = size;
    }


    public void updateFast(String s) {
//        Log.d(LOG_TAG,"fast update: board id="+id+" text="+s + " length=" + s.length());
        if (cellSizeLimit == 0) return;

        Rect bounds = new Rect();
        Paint textPaint = getPaint();
        textPaint.getTextBounds(s, 0, s.length(), bounds);
        int height = bounds.height();
        int width = bounds.width();
        int padding = getPaddingLeft() * 2;
//        Log.d(LOG_TAG, "update char: " + s + " w,h=" + width + "," + height);
        if (width == 0 || height == 0) {
            Log.d(LOG_TAG, "character missing in the font : " + s + " : " + stringToCodepoint(s));
            setText("??");
            return;
        }
        setWidth((int) cellSizeLimit);
        setMinWidth((int) cellSizeLimit);
        setText(s);
    }

    public void update(final String s) {
        //Log.d(LOG_TAG,"fade update: board id="+id+" text="+s + " length=" + s.length());
        // Start from 0.1f if you desire 90% fade animation; 0 if 100%
        final Animation fadeIn = new AlphaAnimation(0.1f, 1.0f);
        fadeIn.setDuration(100);
        //fadeIn.setStartOffset(3000);
        // End to 0.1f if you desire 90% fade animation
        final Animation fadeOut = new AlphaAnimation(1.0f, 0.1f);
        fadeOut.setDuration(100);
        //fadeOut.setStartOffset(3000);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                // start fadeIn when fadeOut ends (repeat)
                updateFast(s);
                startAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(fadeOut);
    }

    public String toString() {
        return id + ": " + this.getText().toString();
    }


}
