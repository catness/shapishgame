package org.catness.shapishgame.elements;


import android.content.Context;
import android.util.AttributeSet;

import org.catness.shapishgame.MainActivity;

public class InputItem extends GradientTextView {
    private static final String LOG_TAG = InputItem.class.getSimpleName();
    private final MainActivity context;
    private float cellSizeLimit = 102f;

    public InputItem(Context context) {
        super(context);
        this.context = (MainActivity) context;
    }

    public InputItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = (MainActivity) context;
    }

    public InputItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = (MainActivity) context;
    }

    public void setCellSizeLimit(float size) {
        cellSizeLimit = size;
    }

    public void updateFast(String s) {
        setWidth((int) cellSizeLimit);
        setMinWidth((int) cellSizeLimit);
        setText(s);
    }

}