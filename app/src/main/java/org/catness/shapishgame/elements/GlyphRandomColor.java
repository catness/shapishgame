package org.catness.shapishgame.elements;

import android.graphics.Color;

import static org.catness.shapishgame.utils.Utils.getRandom;

/**
 * Created by catness on 6/8/19.
 */

public class GlyphRandomColor {
    private static final String LOG_TAG = GlyphRandomColor.class.getSimpleName();
    private int color1, color2;
    private boolean up1, up2;
    private static final int colorMin = 0x50;
    private static final int colorMax = 0xff;
    private static final int step = 10;

    public GlyphRandomColor() {
        color1 = getRandom(colorMin, colorMax);
        color2 = getRandom(colorMin, colorMax);
        up1 = getRandom(0, 1) != 0;
        up2 = getRandom(0, 1) != 0;
    }

    public int getColor1() {
        return Color.rgb(0, color1, 0xff);
    }

    public int getColor2() {
        return Color.rgb(0, color2, 0xff);
    }

    public void update() {
        if (up1) {
            color1 += step;
            if (color1 > colorMax) {
                color1 = colorMax;
                up1 = false;
            }
        } else {
            color1 -= step;
            if (color1 < colorMin) {
                color1 = colorMin;
                up1 = true;
            }
        }
        if (up2) {
            color2 += step;
            if (color2 > colorMax) {
                color2 = colorMax;
                up2 = false;
            }
        } else {
            color2 -= step;
            if (color2 < colorMin) {
                color2 = colorMin;
                up2 = true;
            }
        }
    }

}
