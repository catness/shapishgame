package org.catness.shapishgame.elements;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

public class GlyphItem extends GradientTextView {
    private final int id;
    private static final String LOG_TAG = GlyphItem.class.getSimpleName();
    private final Context context;
    private float cellSizeLimit = 102f;

    public GlyphItem(Context context, int id) {
        super(context);
        this.id = id;
        this.context = context;
    }

    public GlyphItem(Context context, AttributeSet attrs, int id) {
        super(context, attrs);
        this.id = id;
        this.context = context;
    }

    public GlyphItem(Context context, AttributeSet attrs, int defStyle, int id) {
        super(context, attrs, defStyle);
        this.id = id;
        this.context = context;
    }

    public void setCellSizeLimit(float size) {
        cellSizeLimit = size;
    }

    public void updateFast(String s) {
//        Log.d(LOG_TAG,"fast update: board id="+id+" text="+s + " length=" + s.length());
        if (cellSizeLimit == 0) return;

        //Rect bounds = new Rect();
        //Paint textPaint = getPaint();
        //textPaint.getTextBounds(s, 0, s.length(), bounds);
        setWidth((int) cellSizeLimit);
        setMinWidth((int) cellSizeLimit);

        setText(s);
    }

    public void update(final String s) {
        //Log.d(LOG_TAG,"fade update: board id="+id+" text="+s + " length=" + s.length());
        // Start from 0.1f if you desire 90% fade animation; 0 if 100%
        final Animation fadeIn = new AlphaAnimation(0.1f, 1.0f);
        fadeIn.setDuration(100);
        //fadeIn.setStartOffset(3000);
        // End to 0.1f if you desire 90% fade animation
        final Animation fadeOut = new AlphaAnimation(1.0f, 0.1f);
        fadeOut.setDuration(100);
        //fadeOut.setStartOffset(3000);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                // start fadeIn when fadeOut ends (repeat)
                updateFast(s);
                startAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(fadeOut);
    }

    public String toString() {
        String colors = super.toString();
        return id + ": " + this.getText().toString() + " " + colors;
    }

}
