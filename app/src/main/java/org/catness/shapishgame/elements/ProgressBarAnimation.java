package org.catness.shapishgame.elements;

import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ProgressBar;

public class ProgressBarAnimation extends Animation {
    private final ProgressBar progressBar;
    private final float from;
    private final float to;
    private boolean progressUp = true;

    public ProgressBarAnimation(ProgressBar progressBar, float from, float to) {
        super();
        this.progressBar = progressBar;
        this.from = from;
        this.to = to;
        if (from > to) progressUp = false;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        float value = (progressUp) ? from + (to - from) * interpolatedTime : from - (from - to) * interpolatedTime;
        progressBar.setProgress((int) value);
    }

}