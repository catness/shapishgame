package org.catness.shapishgame.elements;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ProgressBar;

import org.catness.shapishgame.InputPanel;
import org.catness.shapishgame.MainActivity;
import org.catness.shapishgame.R;

import static org.catness.shapishgame.utils.Constants.ANIM_DURATION;
import static org.catness.shapishgame.utils.Constants.ANIM_DURATION_MIN;
import static org.catness.shapishgame.utils.Constants.ANIM_SPEEDUP;
import static org.catness.shapishgame.utils.Constants.SLOW_SPEED_COEFF;
import static org.catness.shapishgame.utils.Constants.SLOW_SPEED_COEFF_DEBUG;
import static org.catness.shapishgame.utils.Constants.SPEED_BONUS;
import static org.catness.shapishgame.utils.Constants.TIMERBAR_MAX;

public class TimerBar {
    private static final String LOG_TAG = TimerBar.class.getSimpleName();
    private final InputPanel inputPanel;
    private final ProgressBar timerBar;
    private ProgressBarAnimation anim;
    private int animDuration = ANIM_DURATION;
    private boolean isSlowSpeed = false;
    private boolean isSlowSpeedDebug = false;
    private float speedBonus = 0;
    private int progressSave = 0;
    private boolean isPaused = false;

    public TimerBar(final MainActivity context, InputPanel inputPanel) {
        this.inputPanel = inputPanel;

        Resources res = context.getResources();
        Drawable drawable = res.getDrawable(R.drawable.progressbar, null);

        timerBar = context.findViewById(R.id.progressBar);
        timerBar.setMax(TIMERBAR_MAX);
        timerBar.setProgressDrawable(drawable);
    }

    public void init() {
        anim = new ProgressBarAnimation(timerBar, TIMERBAR_MAX, 0);
        updateDuration(animDuration);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                anim.cancel();
                timerBar.clearAnimation();
                inputPanel.timerEnded();
            }
        });
    }

    private void init(int start) {
        anim = new ProgressBarAnimation(timerBar, start, 0);
        int animDurationPause = start * ANIM_DURATION / TIMERBAR_MAX;
        updateDuration(animDurationPause);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation arg0) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation arg0) {
                anim.cancel();
                timerBar.clearAnimation();
                inputPanel.timerEnded();
            }
        });
    }


    public void restart() {
        animDuration = ANIM_DURATION;
        speedBonus = 0;
        timerBar.setProgress(TIMERBAR_MAX);
        init();
    }

    public void reset() {
        init();
        timerBar.setProgress(TIMERBAR_MAX);
        timerBar.setVisibility(View.VISIBLE);
    }

    private void updateDuration(int animDurationCurrent) {
        int duration;
        if (isSlowSpeedDebug) duration = animDurationCurrent * SLOW_SPEED_COEFF_DEBUG;
        else
            duration = (isSlowSpeed) ? animDurationCurrent * SLOW_SPEED_COEFF : animDurationCurrent;
        //Log.d(LOG_TAG, "update duration=" + duration);
        anim.setDuration(duration);
    }

    public void speedUp() {
        if (animDuration > ANIM_DURATION_MIN) {
            animDuration -= ANIM_SPEEDUP;
            speedBonus += SPEED_BONUS;
            inputPanel.updateSpeedBonus(speedBonus);
        }
        //Log.d(LOG_TAG, "speedup:" + animDuration);
        updateDuration(animDuration);
    }

    public void activate() {
        timerBar.startAnimation(anim);
    }

    public void hide() {
        anim.cancel();
        timerBar.clearAnimation();
        timerBar.setVisibility(View.GONE);
    }

    public void pause() {
        isPaused = true;
        //Log.d(LOG_TAG, "\n\n\n\n\n~~~~~~~~~~~~~~~ progress: " + timerBar.getProgress() + "   ");
        progressSave = timerBar.getProgress();
        anim.cancel();
        timerBar.clearAnimation();
        timerBar.setProgress(progressSave);
    }

    public void resume() {
        isPaused = false;
        //Log.d(LOG_TAG, "saved progress: " + progressSave);
        init(progressSave);
        activate();
    }

    public void setSlowSpeed(boolean isSlowSpeed) {
        this.isSlowSpeed = isSlowSpeed;

        int duration = (isSlowSpeed) ? animDuration * SLOW_SPEED_COEFF : animDuration;
        anim.setDuration(duration);
        if (!isPaused) {
            reset();
            activate();
        }
    }

    public void setSlowSpeedDebug(boolean isSlowSpeedDebug) {
        this.isSlowSpeedDebug = isSlowSpeedDebug;
        if (!isPaused) {
            reset();
            activate();
        }
    }

}
