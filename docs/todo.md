# Done

[x] Refactor the game logics - move from the MainActivity to InputPanel

[x] Change the colors: text, frame, maybe gradient - to make it like the previous version (brighter)

[x] Move the colors and hardcoded strings to the resources

[x] Refactor the double progress bar into one class

[x] Fix the wide fonts

[x] Hint mode for debugging

[x] Fix the grid width so the cells don't jump

[x] Settings menu :
  [x] Debug mode (non-random board chars)
  [x] Select color of the characters: gradient from/to
  [x] Select current font

[x] Display powerups

[x] Powerups - implement their usage
  [x] Double score (on timer)
  [x] Life
  [x] Hint
  [x] Slowdown
  [x] Reset 

[x] Disallow powerup timer animation if the counter is 0; decrement the counter upon animation start

[x] Add counter to powerup; display powerup symbol with the color scale

[x] Get a random powerup upon completing 5 chars

[x] Change the list of fonts in the preferences dynamically when the font is added in the code

[x] Change the score/maxscore to a visual display (progress bar / stars?)

[x] refactor isSlowSpeed and isSlowSpeedDebug in the timer

[x] fix the taiviet font

[x] inputitem - cellsizelimit set dynamically

[x] add horiz padding for input items

[x] maybe more horiz padding for some fonts (hangul)

[x] fontsize 35/25sp - organize it as variables

[x] return to inputcol=1 after losing a life

[x] Change icons for settings

[x] Fix the bug when the gameover message doesn't disappear

[x] Make the hint visually more interesting

[x] Change the layout (extend the powerups all the way to the bottom)

[x] Add new fonts: 
	[x]	katakana
	[x] dingbats
	[x] alchemical (Symbola.ttf)
	[x] runic
	[x] balinese

[x] Increase the speed of the timer after after every board reset

[x] Add an indicator of current character with both gradients to settings

[x] Fix the double progressbar to make it a regular one, using a layered drawable

[x] Find a nice launcher icon

[x] Add the database (Room/LiveData)

[x] Save the bonuses into the database, restore on reload

[x] Update the bonuses color scale to make it more visible when they are added

[x] Dex activity, grid adapter for the dex

[x] Open the web browser for the character info (https://graphemica.com/CHAR)

[x] Improve the dex layout (black bg, borders between the chars)

[x] pause mode on android (temporarily for debugging - a menu option?)

[x] On GameOver, assign a few chars from the screen to the "Dex" randomly and save to the database

[x] Use the amount info from the livedata

[x] dex display: if character doesn't exist yet then a shadow ; else: image, stars (how many times encountered), link to the character info

[x] remove nonexistent chars from Armenian font

[x] add utf-16 fonts to the db (calculate start and end differently)

[x] Debugging option to add/remove chars to the dex manually

[x] check all the glyphs for nonexistent chars and remove them (afterwards, reinit the database)

[x] Gameover alert dialog 

[x] Score bonus when clicking on the glyphs which the user aready has in the dex

[x] Add more levels for keys (golden, emerald)

[x] More stars to the dex after completing 5 silver stars : golden, emerald

[x] Add even more levels for keys and stars before emerald (ruby, sapphire)

[x] Endgame (when all the keys are collected)

[x] Different messages for gameover dialog

[x] Increase the score modifier as the timer bar speed increases

[x] Animated text on gameover win (stars changing color in the status panel?)

[x] Starting screen, with the loading bar until all the fonts are saved in the db

[x] Sound effects, a setting for sound on / off

[x] Code refactoring of the stars/keys levels in StatusPanel and DexAdapter 

[x] Add the settings menu to the dex activity

[x] try to fix the progress bar on pause/resume

[x] Add fonts: Georgian, Javanese

[x] Code refactoring into directories

[x] Add 1 more powerup: pause (instead of the pause on the toolbar)

[x] pause always available (removes a key)

[x] Color animation on loading screen

[x] Fast win - a debugging option

[x] Replace the other fonts with Noto fonts? https://www.google.com/get/noto/

[x] Check if the number of fonts can be reduced because some of them contain more than 1 unicode group

[x] Lint checks

[x] register and unregister the listener in the appropriate methods, like onResume and onPause:
   http://androidtechnicalblog.blogspot.com/2014/02/listening-to-preference-changes.html
   (ok, checked, it doesn't work)

[x] Check what happens with onStart/onStop/onPause/onDestroy : need to save/restore the state 
   (no, it doesn't make sense)

[x] Get rid of hardcoded colors and strings

[x] Get rid of extra debugging printouts

# Maybe

* Add more fonts:  thai, domino? cyrillic? cypriot? arabic?, hebrew? nyanmar, linear A, linear B, Oriya, Osage, Osmanya, Saurashtra, Sinhala, Tifinagh, Unified Canadian Aboriginal Syllabics? khmer? lao?

* Fix the color thumbnail preview properly, using the source code

* A mode with symbol translations 

* Swipe (or double press or other gesture) the powerup to toggle between the symbol and the count

* Style the title of sample text in the preferences so it looks the same as the other preferences

* Customize the scrollbar appearance (the custom scrollbar style doesn't work with fast scrollbar)
